'''
Created on Nov 23, 2015

@author: okovalen
'''
from PyQt5.QtWidgets import QWidget, QPushButton, QVBoxLayout, QHBoxLayout,\
    QLabel
    
from PyQt5.QtCore import Qt # for Qt.WA_DeleteOnClose
    
#from MainWindow import mainWindow
    
import config
import configparser


class StartupWindow(QWidget):
    def __init__(self):
        super().__init__()
        self.initUI()
    
    def initUI(self):
        self.setWindowTitle('Message')
        #self.resize(300, 200)
        self.setAttribute(Qt.WA_DeleteOnClose)
        self.setWindowFlags(Qt.WindowStaysOnTopHint)
        
        text = 'Make sure that you have the following files in the working folder:\n' \
            '1) "config.ini"\n' \
            '2) "madx.exe"\n' \
            '3) "CR_lattice"\n' \
            '4) "CR_work"\n' \
            '5) "twiss.txt"\n' \
            'Note that the names of all the above files can be changed in the "config.ini".\n' \
            'The twiss file should have the following fields:\n' \
            '"S", "BETX", "BETY", "DX" for the main plot of the optical functions and ' \
            '"L", "K1L" for the top sub-plot of the magnets'
            
        
        label = QLabel(text)
        label.setWordWrap(True)
        
        buttonOk = QPushButton('Ok')
        buttonOk.clicked.connect(self.buttonOkClicked)
        
        buttonLater = QPushButton('Do not show anymore')
        buttonLater.clicked.connect(self.buttonLaterClicked)
        
        
        
        hbox = QHBoxLayout()
        hbox.addWidget((buttonOk))
        hbox.addWidget(buttonLater)
        
        vbox = QVBoxLayout()
        vbox.addWidget(label)
        vbox.addLayout(hbox)
        
        self.setLayout(vbox)
        
    def buttonOkClicked(self):
        print("Ok")
        self.close()
        
    def buttonLaterClicked(self):
        print("later")
        config.Config['Startup window']['show'] = '0'
        config.Config.write()
        #=======================================================================
        # config.Config.remove_option('Startup window', 'show')
        # with open(config.configFile, 'w') as f:
        #     config.Config.write(f)
        # 
        # newConfig = configparser.ConfigParser()
        # newConfig.add_section('Startup window')
        # newConfig.set('Startup window', 'show', 'False')
        #  
        # with open(config.configFile, 'a') as f:
        #     config.Config.write(f)
        #=======================================================================
        self.close()

