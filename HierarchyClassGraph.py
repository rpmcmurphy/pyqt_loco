'''
Created on 9 Oct 2015

@author: localadmin_okovalen
'''

from pycallgraph import PyCallGraph
from pycallgraph.output import GraphvizOutput

from Calculations import calculations

class hierarchyClassGraph(object):
    def __init__(self):
        with PyCallGraph(output=GraphvizOutput()):
            print("Creating pdf file with class hierarchy...")
            calc = calculations()
            calc.runORM()
            print("pycallgraph.pdf file should be created by now!")
    
        