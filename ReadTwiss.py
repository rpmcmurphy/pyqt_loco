import io
import numpy as np
import config
#import matplotlib.pyplot as plt

class readTwiss():
    """ 
    Fields in the twiss file:
    NAME, KEYWORD, S, L, K1L, ALFX, BETX, MUX, DX, DPX, ALFY, BETY, MUY, X, Y 
    """
    def __init__(self, fname):
        self.fname = fname
        self.latticeElements = {'quad': None, 'sext': None, 'kick': None, 'mon': None}
        self.optics = {'Q1': None, 'Q2': None, 'BETXMAX': None, 'BETYMAX': None, 'DQ1': None, 'DQ2': None, 'LENGTH': None}
        self.data, self.latInfo = [], []
        #self.readFile(fname)
               
    def readFile(self, fname):
        with open(fname, 'r') as fin:
            data = fin.readlines()
        if not data:
            raise Exception("twiss file is empty!")
        return data
    
    def checkFields(self):
        try:
            self.data['S']
        except:
            raise Exception("We do not have S (position) field in"\
                            'the twiss file.\nPlease add the '\
                            'corresponding command into the '\
                            'file for mad execution and relaunch'\
                            'madx.exe')

        try:
            self.data['BETX']
        except:
            raise Exception("We do not have BETX field in"\
                            'the twiss file.\nPlease add the '\
                            'corresponding command into the '\
                            'file for mad execution and relaunch'\
                            'madx.exe')  
            
        try:
            self.data['BETY']
        except:
            raise Exception("We do not have BETY field in"\
                            'the twiss file.\nPlease add the '\
                            'corresponding command into the '\
                            'file for mad execution and relaunch'\
                            'madx.exe')  
            
        try:
            self.data['DX']
        except:
            raise Exception("We do not have DX field in"\
                            'the twiss file.\nPlease add the '\
                            'corresponding command into the '\
                            'file for mad execution and relaunch'\
                            'madx.exe')  

        try:
            self.data['DY']
        except:
            raise Exception("We do not have DY field in"\
                            'the twiss file.\nPlease add the '\
                            'corresponding command into the '\
                            'file for mad execution and relaunch'\
                            'madx.exe')  
            
        try:
            self.data['X']
        except:
            raise Exception("We do not have X (position) field in"\
                            'the twiss file.\nPlease add the '\
                            'corresponding command into the '\
                            'file for mad execution and relaunch'\
                            'madx.exe')
            
        try:
            self.data['Y']
        except:
            raise Exception("We do not have Y (position) field in"\
                            'the twiss file.\nPlease add the '\
                            'corresponding command into the '\
                            'file for mad execution and relaunch'\
                            'madx.exe')

    
    def readTwissFile(self, fname):
        #twiss = readTwiss(fname) # Create an instance of a class
        #textRaw = twiss.readFile(r'MAD\twiss.txt')
        textRaw = self.readFile(fname)
        # Clean text from junk and extract data
        dataRaw, latInfoRaw = self.removeJunk(textRaw)
        # Obtain clean data
        self.data = self.textToMatrix(dataRaw)
        self.checkFields()
        #print(self.data)
        self.latInfo = self.latInfoClean(latInfoRaw) # self.latInfo is a dict
        return self.data, self.latInfo
        
    def numberOfMagnElements(self):
 # read twiss.txt to determine the number of magnetic elements
        data = self.readFile(self.fname)
        mon, quad, sext, kick = 0, 0, 0, 0
        for line in data:
            if "MONITOR" in line:
                mon += 1
                # print("Monitor" + str(i))
            elif "QUADRUPOLE" in line:
#                 if float(line.split()[3]) != 0:
                    quad += 1
            elif "SEXTUPOLE" in line:
                sext += 1
            elif "KICK" in line:
                kick += 1
#             self.setLabelText(self.lblQuadNum, quad)
#             self.setLabelText(self.lblSextNum, sext)
#             self.setLabelText(self.lblKickNum, kick)
#             self.setLabelText(self.lblMonNum, mon)
        
        self.latticeElements['quad'] = quad
#         if quad == 0:
#             raise Exception('We assume the twiss command: select, flag=twiss, column=name,'\
#                         ' keyword, s, k1l ... We also count the quadrupoles whose strengths'\
#                         'are not zero! See function "numberOfMagnElements"')
        self.latticeElements['sext'] = sext
        self.latticeElements['kick'] = kick
        self.latticeElements['mon'] = mon
        
#===============================================================================
#         def OpticalParamsInfo():
#             pass
# #             self.setLabelText(self.lblTuneXVal, self.latInfo['Q1'])
# #             self.setLabelText(self.lblTuneYVal, self.latInfo['Q2'])
# #             self.setLabelText(self.lblBetaXMaxVal, self.latInfo['BETXMAX'])
# #             self.setLabelText(self.lblBetaYMaxVal, self.latInfo['BETYMAX'])
# #             self.setLabelText(self.lblChromXVal, self.latInfo['DQ1'])
# #             self.setLabelText(self.lblChromYVal, self.latInfo['DQ2'])
# #             self.setLabelText(self.lblLengthVal, self.latInfo['LENGTH'])
#             
# #             self.optics['Q1'] = self.latInfo['Q1']
# #             self.optics['Q2'] = self.latInfo['Q2']
# #             self.optics['BETXMAX'] = self.latInfo['BETXMAX']
# #             self.optics['BETYMAX'] = self.latInfo['BETYMAX']
# #             self.optics['DQ1'] = self.latInfo['DQ1']
# #             self.optics['DQ2'] = self.latInfo['DQ2']
# #             self.optics['LENGTH'] = self.latInfo['LENGTH']
#===============================================================================
    
        #OpticalParamsInfo()
        
        #return self.latticeElements, self.optics
        return self.latticeElements
       

   
    def removeJunk(self, text):
        """ Remove unneeded lines """
        headerInfo = []
        dataNew = []
        for i, val in enumerate(text):
            if val[0] == '$':
                pass # we do not need these commented lines
            elif val[0] == '@':
                headerInfo.append(val)
            elif val[0] == '*':
                dataNew.append(val.replace('*', ' '))
            else:
                dataNew.append(val)
        return dataNew, headerInfo
    
    def arrayToBytes(self, array):
        """ This function is needed only for genfromtxt """
        stream = ''.join(array)
        text_bytes = stream.encode('utf-8')
        return text_bytes
    
    def textToMatrix(self, text):
        """genfromtxt uses by default dtype = float, so one should use
        dtype = None so genfromtxt guesses the format. After that
        one should access the data by data['f0'].
         genfromtxt expects the byte stream rather than unicode,
        so one has to transform (encode) utf to byte. Then one uses
        io.BytesIO
        
        Here we also lose the first line 'name, l, k1l...'
        """
        # join list to obtain a continuous stream
        
        #=======================================================================
        # text = ''.join(text)
        # text = 'a b c\n1 2 3\n4 5 6'
        # print(type(text), text)
        # data = np.genfromtxt(io.StringIO(text), names = True, dtype = None)
        # print(data)
        #=======================================================================
        
        text_bytes = self.arrayToBytes(text)
        # Convert tot matrix with the access as table['col1']
        # The first names-line now disappears
        data = np.genfromtxt(io.BytesIO(text_bytes), names = True, dtype = None)
        return data
    
    #===========================================================================
    # def bytesToType(self, data):
    #     """ This function is needed to decode from bytes to string(float) after genfromtxt.
    #     a multidimensional (e.g., Nx2) array is expected as an input """
    #     rows = len(data)
    #     cols = len(data[0])
    #     dataNew = np.empty([rows, cols])
    #     for row in range(rows):
    #         for col in range(cols):
    #             tmp = data[row][col].astype(str)
    #             if (tmp == None): # when it is float, not a string
    #                 tmp = data[row][col].astype(float)
    #             dataNew[row][col] = tmp
    #===========================================================================
    
    def latInfoClean(self, text):
  
        textBytes = self.arrayToBytes(text)
        dataBytes = np.genfromtxt(io.BytesIO(textBytes), dtype = None, usecols = (1,3))
        
        def ByteToType(bytesData):
            """ bytesData of shape 2xn is assumed with bytes values, which 
                can be converted either to string or to float"""
            from collections import OrderedDict

            def isByteFloat(s):
                """ Checks whether the Byte type value (after genfromtxt use)
                     can be converted to float """
                try:
                    s.astype(float)
                    return True
                except ValueError:
                    return False
            def isByteString(s):
                """ Checks whether the Byte type value (after genfromtxt use)
                     can be converted to string """
                try:
                    s.astype(str)
                    return True
                except ValueError:
                    return False
            
            """ self.latInfo is a Nx2 array of parameters. The first column is the names,
            second is the values. We need to perform search of the value by its name.
            The self.latticeInfo is in bytes, so we need to precede the parameter 
            name with "b" """
            parNamesBytes, parValsBytes = bytesData[:, 0], bytesData[:, 1]
            # idxFromName = lambda ParName: np.where(parNamesBytes == ParName)
            # LengthBytes = parValsBytes[idxFromName(b'LENGTH')]
            # Length = LengthBytes.astype(float)
            bytesDict = OrderedDict(zip(parNamesBytes, parValsBytes))
            parNames, parVals = [], []
            for key, value in bytesDict.items():
                # change value from bytes to string/float
                #print('before', key, value)
                if isByteFloat(value):
                    parVals.append(value.astype(float))
                elif isByteString(value):
                    parVals.append(value.astype(str))
                # same for keys(names)
                if isByteFloat(key):
                    parNames.append(key.astype(float))
                elif isByteString(key):
                    parNames.append(key.astype(str))
                    
            convertedDict = OrderedDict(zip(parNames, parVals))
            return convertedDict
            
        #print('before', dataBytes)
        
        data = ByteToType(dataBytes)
        
        #for key, value in data.items(): print('after', key, value)

        return data
    



if __name__ == '__main__':
    twiss = readTwiss(config.twissFile) # Create an instance of a class
    textRaw = twiss.readFile(config.twissFile)
    # Clean text from junk and extract data
    dataRaw, latInfoRaw = twiss.removeJunk(textRaw)
    
    # Obtain clean data
    data = twiss.textToMatrix(dataRaw)
    latInfo = twiss.latInfoClean(latInfoRaw)
    
    # Plot data
    import matplotlib.pyplot as plt
    def plotTwiss(s, bx, by, dx):
        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.plot(s, bx, 'r', label = 'Betx')
        ax.plot(s, by, 'b', label = 'Bety')
        ax.grid()
        ax1 = ax.twinx()
        ax1.plot(s, dx, 'g', label = 'Dx')
        ax.legend(loc = 1)
        ax1.legend(loc = 2)
    
        plt.show()
        
    plotTwiss(data['S'], data['BETX'], data['BETY'], data['DX'])
