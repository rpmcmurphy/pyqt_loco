'''
22.07.15:
Decided to put all the files (madx, bat, lattice) in one directory, where
the file main.py is located. Otherwise I need to put the FULL path in file
.lattice where I create the files, e.g. twiss.txt, since by default it creates
all the files in the directory of the file main.py from which I call the madx...



Created on 17 Jul 2015

@author: localadmin_okovalen
'''

from PyQt5.QtWidgets import QApplication

import sys
# add the source directory with py files to the PATH
#sys.path.append('source')
#from CalcThread import calcThread
#from Calculations import calculations
from MainWindow import mainWindow
#from PyQt5.Qt import QStyleFactory

CLASS_HIERARCHY_GRAPH = False # create pycallgraph.pdf file with class call hierarchy
DEBUG = False

#FIXME: when there is not line "call, file = "CR_work.txt; stop;" the lattice does not call the file.
#FIXME: Add this line if it is not found in the file

#===============================================================================
# def createChildWindow():
#     wnd = mainWindow()
#     wnd.show()
# asdf
#===============================================================================

if __name__ == "__main__":
    if CLASS_HIERARCHY_GRAPH:
        
        from HierarchyClassGraph import hierarchyClassGraph
        callClassGraph = hierarchyClassGraph()
            
    #QApplication.setStyle(plastique)
    #QApplication.setStyle('plastique')
    #QApplication.setStyle(QStyleFactory.create('plastique'))
    #QApplication.setStyle(QStyleFactory.create('windows'))
    app = QApplication(sys.argv)
    #QApplication.setStyle(QStyleFactory.create('cleanlooks'))
    #app.setStyle(QStyleFactory.create('plastique'))
    #app.setStyle("plastique")
        
    mw = mainWindow(DEBUG)
    mw.show()
    
    #===========================================================================
    # if config.showStartup == '1':
    #     startupWindow = StartupWindow()
    #     startupWindow.show()
    #===========================================================================
    
    sys.exit(app.exec_())