'''
Created on 23 Oct 2015

@author: localadmin_okovalen
'''

import os
import config
import numpy as np

class madxExecution(object):
    def __init__(self):
        pass
    
    def launchBat(self, launchBatName):
        """ look for the current working directory (cwd) issues in 
        1) Launch.bat (put ~dp0 )
        2) in the first file which calls the second one (put full path) """
        import subprocess
        pathBat = os.path.join(os.getcwd(), launchBatName)
        #pathBat = os.getcwd() + '\\' + self.folder + '\\' + launchBatName  
        # Launch.bat: %~dp0\madx < %~dp0\Hesr_CR_injection_StSx.txt
        # %~dp0 means like current working directory
        #print(pathBat)
        
        # suppress output in the Eclipse window
        with open(os.devnull, "w") as fnull:
                result = subprocess.call(pathBat, stdout = fnull, stderr = fnull)
        
        # check if the output "dump.txt" exists
        #if os.path.exists("C:/Users/localadmin_okovalen/Documents/EclipseWork/LOCO/dump.txt") == 1:
        #    print("Yes, we have dump.txt")
            
        # check if "dump.txt" contains no warnings:
        
        def checkWarnings():      
            with open(config.dumpFile, "r") as fout:
                if "Number of warnings: 0" in fout.read():
                    pass
                else:
                    raise Exception("We have warnings in Twiss file !!! Look at the 'dump.txt' file.")
                                    #"\n Note that it will be closed "
                                    #"after closing the main program window")
        
        checkWarnings()

        # do not suppress output
        # subprocess.call(pathBat)

#         #with open (self.folder + '/twiss.txt') as fin:
#         with open (self.folder + twissName) as fin:
#             data = fin.readlines()
#         #print(data)
        
        message = 'Execute ' + launchBatName
        print(message)
        #TODO: Think on how to implement the message to the statusBar after the functions of the calculation class.
        # self.statusBarMessage(message)
        
    def launchMadx(self):
        self.launchBat(config.launchBatFile)
        
    def deleteFile(self, fname):
        if os.path.isfile(fname):
            os.remove(fname)

    
    def deleteNewFiles(self):
        #fileNames = ['Launch', 'Hesr_CR_injection_StSx', 'HesrAperture_CR_core_StSx', 'twiss']
        configFirstFileName, configFirstFileExt = config.first.split('.')  # @UndefinedVariable
        configSecondFileName, configSecondFileExt = config.second.split('.')  # @UndefinedVariable
        fileNames = ['Launch', configFirstFileName, configSecondFileName, 'twiss']
        endings = ['_exp', '_tmp', '_new']
        
        # remove files coming from varying kickers
        kickStrengths = np.loadtxt(config.kickFile, dtype = bytes).astype(str)
        for val in kickStrengths:
            endings.append('_' + val)
            
            
        extensions = ['.txt', '.bat']
        
        for ending in endings:
            for name in fileNames:
                for ext in extensions:
                    fullName = name + ending + ext
                    self.deleteFile(fullName)
        
        # Delete dump.txt
        #=======================================================================
        # dumpFile = 'dump.txt'
        # if os.path.isfile(dumpFile):               
        #     os.remove(dumpFile)
        #=======================================================================
        
    def createLaunchBat(self, launchName, callableFile):
        #print(os.path.join(os.getcwd(), launchName))
        if not os.path.exists(os.path.join(os.getcwd(), launchName)):
            with open (launchName, 'w') as f:
                f.write("%~dp0\\madx < %~dp0\\" + callableFile + ' > dump.txt')
                
                
if __name__ == '__main__':
    fileManager = madxExecution()
    fileManager.deleteNewFiles()
        
            
        