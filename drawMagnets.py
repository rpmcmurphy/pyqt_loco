'''
Created on 13 Nov 2015

@author: localadmin_okovalen
'''

import config
from ReadTwiss import readTwiss
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.figure import Figure
import matplotlib.patches as patches

class DrawMagnets(object):
    def __init__(self):
        twissFile = config.twissFile
        twiss = readTwiss(twissFile)
        self.data, latInfo = twiss.readTwissFile(twissFile)
        
        self.posArray = []
        self.lengthArray = []
        self.strengthArray = []
        self.typeArray = []
        self.nameArray = []
        
        
        
        
    def parseTwiss(self):
        #FIXME: Move all this to parseTwiss or readTwiss...
        #FIXME: self.data is enough in principle
        try:
            self.posArray = self.data['S']
        except:
            raise Exception("We do not have S (position) field in"\
                            'the twiss file.\nPlease add the '\
                            'corresponding command into the '\
                            'file for mad execution and relaunch'\
                            'madx.exe')
            
        try:
            self.lengthArray = self.data['L']
        except:
            raise Exception("We do not have L (length) field in"\
                            'the twiss file.\nPlease add the '\
                            'corresponding command into the '\
                            'file for mad execution')
            
        try:
            self.strengthArray = self.data['K1L']
        except:
            raise Exception("We do not have K1L (quad strength) field in"\
                            'the twiss file.\nPlease add the '\
                            'corresponding command into the '\
                            'file for mad execution and relaunch'\
                            'madx.exe')
            
        try:
            self.typeArray = self.data['KEYWORD']
        except:
            raise Exception("We do not have KEYWORD (magnet type) field in"\
                            'the twiss file.\nPlease add the '\
                            'corresponding command into the '\
                            'file for mad execution and relaunch'\
                            'madx.exe')

        try:
            self.nameArray = self.data['NAME']
        except:
            raise Exception("We do not have NAME (magnet name) field in"\
                            'the twiss file.\nPlease add the '\
                            'corresponding command into the '\
                            'file for mad execution and relaunch'\
                            'madx.exe')
            
            
    def defineRectangles(self):
        self.parseTwiss()
        xleft = []
        xright = []
        yBot = []
        yTop = []
        facecolor = []
        alpha = []
        #FIXME: Right now not all the magnets are plotted. That means that we 
        #FIXME: cannot click some magnets (markers) at the main Window magnets plot
        #FIXME: This is only done as a workaround of cycle in onClick() of MainWindow
        plotMagnNames = [] # The names of the magnets which are plotted (not all)
        plotMagnTypes = [] # The types of the magnets which are plotted (not all)
        alphaDefault = 1.0
        # hatches = [] # '-' or '/' or ...
        quadSize = 10
        dipSize = quadSize*0.33
        sextSize = quadSize*0.66
        kickSize = 1.33*quadSize
        monSize = kickSize
        for s, l, k, type, name in zip(self.posArray, self.lengthArray, 
                           self.strengthArray, self.typeArray, self.nameArray):
            if l == 0: # for kickers
                l = 0.1 # make it at least visible
    
            
            if type == b'"QUADRUPOLE"' and float(k) != 0:
                xleft.append(s - l/2)
                xright.append(s + l/2)
                yBot.append(-1*quadSize)
                yTop.append(quadSize)
                if float(k) > 0:
                    facecolor.append('red')
                elif float(k) < 0:
                    facecolor.append('blue')
                alpha.append(alphaDefault)
                plotMagnNames.append(name)
                plotMagnTypes.append(type)

                    
            elif type == b'"SBEND"':
                xleft.append(s - l/2)
                xright.append(s + l/2)
                yBot.append(-1*dipSize)
                yTop.append(dipSize)
                facecolor.append('cyan')
                alpha.append(0.5)
                plotMagnNames.append(name)
                plotMagnTypes.append(type)
                
            elif type == b'"SEXTUPOLE"':
                xleft.append(s - l/2)
                xright.append(s + l/2)
                yBot.append(-1*sextSize)
                yTop.append(sextSize)
                facecolor.append('magenta')
                alpha.append(alphaDefault)
                plotMagnNames.append(name)
                plotMagnTypes.append(type)
                
            elif type in [b'"KICKER"', b'"HKICKER"', b'"VKICKER"']:
                xleft.append(s - l/2)
                xright.append(s + l/2)
                if type == b'"KICKER"':
                    yBot.append(-1*kickSize)
                    yTop.append(kickSize)
                elif type == b'"HKICKER"':
                    yBot.append(-1*kickSize)
                    yTop.append(0)  
                elif type == b'"VKICKER"':
                    yBot.append(0)
                    yTop.append(kickSize)  
                facecolor.append('black')
                alpha.append(alphaDefault)
                plotMagnNames.append(name)
                plotMagnTypes.append(type)
                
            elif type in [b'"MONITOR"', b'"HMONITOR"', b'"VMONITOR"']:
                xleft.append(s - l/2)
                xright.append(s + l/2)
                if type == b'"MONITOR"':
                    yBot.append(-1*kickSize)
                    yTop.append(kickSize)
                elif type == b'"HMONITOR"':
                    yBot.append(-1*kickSize)
                    yTop.append(0)  
                elif type == b'"VMONITOR"':
                    yBot.append(0)
                    yTop.append(monSize)  
                facecolor.append('green')
                alpha.append(alphaDefault)
                plotMagnNames.append(name)
                plotMagnTypes.append(type)
                
        rectangles = [patches.Rectangle(
                        (xleft[i], yBot[i]), xright[i] - xleft[i], yTop[i] - yBot[i],
                         facecolor = facecolor[i], edgecolor = 'none', alpha = alpha[i])
                      for i in range(len(yBot))]
        return rectangles, plotMagnTypes, plotMagnNames
        
    def draw(self):
        """ it is not used except in this module in main """
#         s  = self.posArray
#         l = self.lengthArray
#         k = self.strengthArray

                
        
        fig = plt.figure(figsize = (10, 2))
        ax = fig.add_subplot(111)
        
        rectangles, types, names = self.defineRectangles()
        for patch in rectangles:
            ax.add_patch(patch)

        ax.set_ylim(-15, 15)
        ax.set_xlim(0, 225)
        plt.show()
            


            
if __name__ == '__main__':
    #twissFile = config.twissFile
    #twiss = readTwiss(twissFile)
    #textRaw = twiss.readFile(twissFile)
#     dataRaw, latInfoRaw = twiss.removeJunk(textRaw)
#     dataClean = twiss.textToMatrix(dataRaw)
#     print(dataClean.dtype.names)
    
    
    #data, latInfo = twiss.readTwissFile(twissFile)
    
    drawMagnets = DrawMagnets()
    #drawMagnets.parseTwiss()
    drawMagnets.draw()
    