'''
Created on 8 Oct 2015

@author: localadmin_okovalen
'''
from PyQt5.Qt import QPalette

STARTMESSAGE = False  # A message that informs that a MAD folder should be created
                    # and a file "twiss.txt" should be in the folder
# DEBUG = True # Debug mode

import sys

import matplotlib
# matplotlib.use("Qt5Agg")
matplotlib.use("Qt5Agg", force=True)
# matplotlib.rcParams['backend'] = 'Qt5Agg'
# print(matplotlib.get_backend())

from PyQt5.QtWidgets import QMainWindow, QApplication, QVBoxLayout, QMessageBox, \
    QFileDialog, QAction, QLabel, QPushButton, QWidget, QHBoxLayout, QGridLayout, \
    QLayout, QGroupBox, QSpacerItem, QSizePolicy, QScrollArea, QTextEdit, QDialog, \
    QDesktopWidget, QTabWidget
from PyQt5 import uic
from PyQt5.QtCore import Qt
from PyQt5.QtCore import QObject
from PyQt5.QtGui import QPalette


from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
import matplotlib.pyplot as plt

import os
import numpy as np

np.set_printoptions(precision=6, suppress=True)  # suppress e+00

from ReadTwiss import readTwiss
from Calculations import calculations
from ParseLatticeFile import parseLattice
from CalcThread import calcThread
from PlotTwiss import plotTwiss
from CalcThread import calcThread
from CalcResultsWindow import calcResultsWindow
from InfoWindow import infoWindow, infoTab
from InfoWindow import infoTab
from ClosedOrbit import closedOrbit
import config
from MadxExecution import madxExecution
from CalcResultsWindow import calcResultsTabInfo
from ClosedOrbit import closedOrbitWindow
from kickersStrengthPlot import KickersStrengthPlotWindow
from startupWindow import StartupWindow
from plotMatrR import histo3dWindow


#===============================================================================
# class Design(Object):
#     def __init__(self):
#         tabs = True
#         windows
#===============================================================================

DESIGN = 'tabs'
# DESIGN = 'windows'


class mainWindow(QMainWindow):
    def __init__(self, debug=False):
        super().__init__()
        
        self.debug = debug  # if true, it will create a separate DEBUG menu in the window
        self.figureBackground = 'lightgray'
        fname = config.twissFile
# TODO: looks like we have two "self.cals" one created here and it is almost obsolete (used only for first twiss plot).
# TODO: another one created by self.thread and it is the main one which does all the calculations
        self.calc = calculations()
        self.readTwiss = readTwiss(fname)
        self.plotTws = plotTwiss()
        self.madxExec = madxExecution()
        
        

# TODO: Too many readTwiss, read, twiss....

        self.readTwiss.numberOfMagnElements()
        self.readTwiss.readTwissFile(fname)
        
        # Here self.calc is needed to connect the menus to the functions like
        # "run MAD-X" or "run ORM"
        self.initUI()
        
        self.showInfo()
        
        
        # self.myInit() has to go AFTER self.initUI() because in the myInit 
        # the labels like lblQuadNum are Used
        # self.myInit()
        

        
        
        # self.plotTws = plotTwiss()
        

        
        # self.setupUi(self) # setup UI-form "untitled.ui"
        
    def initUI(self):
        def setupStartupWindow():
            if config.showStartup == '1':
                # self.startupWindow = StartupWindow()
                # self.startupWindow.show()
                self.startupWindow = StartupWindow()
                self.startupWindow.show()
            
            # self.startWindow = StartupWindow()
        """ Setup User Interface - Window, buttons, menus etc. """
        def setupWindow():
            self.setWindowTitle("Lattice engine. Copyright Oleksandr Kovalenko")
            self.setWindowGeometry(desktopRatio=0.7, offsetPxl=100)
        
        def setupCentralWidget():
            centralWidget = QWidget()
            # centralWidget.setStyleSheet('background-color: #f1f1f1')
            # centralWidget = QPushButton('asdf')
            self.centralLayout = QHBoxLayout()
            centralWidget.setLayout(self.centralLayout)
            self.setCentralWidget(centralWidget)
            
            self.tabs = QTabWidget()
            self.tabs.setTabsClosable(True)
            self.tabs.tabCloseRequested.connect(self.closeTab)
            
            
            # centralLayout.addWidget(self.tabs)
            
            # self.tabs.setStyleSheet('background-color: blue')
                        
            
            # self.setCentralWidget(self.tabs)
#             self.setCentralWidget(centralWidget)
            
            
            
            #===================================================================
            # centralWidget = QWidget()
            # self.setCentralWidget(centralWidget)
            # # add layout to central widget
            # self.layout = QGridLayout()
            # centralWidget.setLayout(self.layout)
            #===================================================================
            
        def onClick(event):
            """ This is needed for tooltips while hovering the mouse over the
            magnets rectangles """
#             print('button=%d, x=%d, y=%d, xdata=%f, ydata=%f'%(
#                 event.button, event.x, event.y, event.xdata, event.ydata))
            
            # print(event.xdata, event.ydata)
            
            x = event.xdata
            y = event.ydata
            
            # TODO: See below
            # self.plotTws.magnetRectangles are the magnets which are drawn
            # (not all magnets). Whereas self.plotTws.magnetNames and
            # self.plotTws.magnetTypes are the magnet names and types of ALL
            # the magnets '''
            
            
            
            flagFound = False
            for (magnName, magnType, magnRect) in zip(
                                self.plotTws.magnetNames,
                                self.plotTws.magnetTypes,
                                self.plotTws.magnetRectangles):
                xLeft = magnRect.get_x()
                xRight = xLeft + magnRect.get_width()
                yBot = magnRect.get_y()
                yTop = yBot + magnRect.get_height()
                if xLeft < x < xRight and yBot < y < yTop:
#                     print(magnName, magnType)
                    # decode from bytes to utf-8
                    magnName = magnName.decode('utf-8')
                    magnName = magnName.strip('"')
                    magnType = magnType.decode('utf-8')
                    magnType = magnType.strip('"')        
                    self.magnetNameVal.setText(magnName)
                    self.magnetTypeVal.setText(magnType)
                    print(magnName, magnType)
                    flagFound = True
                    break
            if not flagFound:
                self.magnetNameVal.setText('Click magnet')
                self.magnetTypeVal.setText('Click magnet')
            # Set text to label
            
                    
            
        
            
        def setupPlotTwiss():
            # self.figure = plt.Figure()
            self.figure = plt.Figure(facecolor=self.figureBackground)
            self.canvas = FigureCanvas(self.figure)  # connect "figure" to "canvas"
            self.canvas.setMinimumSize(400, 400)
            # we need this for tooltip on mouse hovering over the magnets
            click = self.canvas.mpl_connect('button_press_event', onClick)
            
            # self.layout.addWidget(self.canvas, 0, 1)
            # self.setCentralWidget(self.canvas)
            
            
            self.plotTws.draw(self.canvas, self.figure, self.calc.data['S'],
                        self.calc.data['BETX'], self.calc.data['BETY'], self.calc.data['DX'])  # open and plot
            
            # self.myFigSave.print_figure("aaa.pdf")
             
            # self.canvas.draw() # draw canvas
             
            # self.layout.addWidget(self.canvas, 1, 0) # add canvas to layout
            # self.setLayout(self.layout)
            # self.tabs.insertTab(0, self.canvas, 'Tab1')
            
            # self.centralLayout.addWidget(self.canvas)
            
        def setupMenuBar():
            # MenuBar
            menubar = self.menuBar()
            
            def fileMenu():
                filemenu = menubar.addMenu('&File')          
                def launchMadxSubMenu():
                    launchMadxAction = QAction('Launch MAD-X', self)
                    launchMadxAction.setShortcut('Ctrl+M')
                    launchMadxAction.triggered.connect(self.madxExec.launchMadx)
                    filemenu.addAction(launchMadxAction)
                def openTwissSubMenu():     
                    openTwissAction = QAction('Open Twiss', self)
                    openTwissAction.setShortcut('Ctrl+O')
                    openTwissAction.triggered.connect(self.openDialog)
                    filemenu.addAction(openTwissAction)
                def saveTwissFigureSubMenu():
                    saveTwissFigureAction = QAction('Save plot', self)
                    saveTwissFigureAction.setShortcut("Ctrl+S")
                    saveTwissFigureAction.triggered.connect(self.saveFigureDialog)
                    filemenu.addAction(saveTwissFigureAction)
                def closeWindowSubMenu():
                    closeWindowAction = QAction('Exit', self)
                    closeWindowAction.setShortcut('Esc')
                    closeWindowAction.triggered.connect(self.closeWindow)
                    filemenu.addAction(closeWindowAction)
                    
                launchMadxSubMenu()
                openTwissSubMenu()
                saveTwissFigureSubMenu()
                closeWindowSubMenu()
 
            def calcMenu():
                # calcMenu
                calcmenu = menubar.addMenu('&Calculations')
                def deleteFilesSubMenu():
                    deleteFilesAction = QAction('Remove temporary files', self)
                    deleteFilesAction.setShortcut('Ctrl+D')
                    deleteFilesAction.triggered.connect(self.madxExec.deleteNewFiles)
                    calcmenu.addAction(deleteFilesAction)
                def runORMSubMenu():
# TODO: do I need to create an object "self.thread" in the menu?
# TODO: Think on adding a QTabWidget for ORM results instead of creating a new window
                    self.thread = calcThread()
                    runORMAction = QAction('&Run ORM analysis', self)
                    runORMAction.setShortcut('Return')
                    
                    if DESIGN == 'windows':
                        # Create info window
                        runORMAction.triggered.connect(self.createCalcWindow)
                    elif DESIGN == 'tabs':
                        # Create separate tab
                        runORMAction.triggered.connect(self.createCalcTab)
                    # Run ORM
                    runORMAction.triggered.connect(self.thread.start)
                    
                    # Visualize ORM matrix
                    if config.plotMatrRdiff:
                        runORMAction.triggered.connect(self.ORMMatrVisualize)
                    
                    calcmenu.addAction(runORMAction)
                def closedOrbitMenu():
                    newAction = QAction('Closed orbit correction', self)
                    newAction.setShortcut('Ctrl+R')
                    newAction.triggered.connect(self.closedOrbitInfo)
                    newAction.triggered.connect(self.closedOrbitKickStrengths)
                    #===========================================================
                    # if DESIGN == 'windows':
                    #     newAction.triggered.connect(self.closedOrbitInfo)
                    # if DESIGN == 'tabs':
                    #     newAction.triggered.connect(self.closedOrbitInfoTab)
                    #===========================================================
                    # newAction.triggered.connect(self.createCalcWindow)
                    calcmenu.addAction(newAction)                    
                    
                deleteFilesSubMenu()
                runORMSubMenu()
                closedOrbitMenu()
            
            def helpMenu():
                helpMenu = menubar.addMenu('&Help')
                def getStartSubMenu():
                    getStartAction = QAction('Getting started', self)
                    getStartAction.setShortcut('Ctrl+H')
                    getStartAction.triggered.connect(self.GettingStartedInfo)
                    helpMenu.addAction(getStartAction)
                    
                getStartSubMenu()
                  
            def setupDebugModeMenu():
                debugMenu = menubar.addMenu('&Debugging tools (not for user)')
                
                def deleteNewFilesMenu():
                    deleteNewFilesAction = QAction('1) Delete temporary files', self)
                    deleteNewFilesAction.triggered.connect(self.madxExec.deleteNewFiles)
                    debugMenu.addAction(deleteNewFilesAction)
                def launchMadxMenu2():
                    launchMadxAction2 = QAction('2) Launch MAD-X', self)
                    launchMadxAction2.triggered.connect(self.madxExec.launchMadx)
                    debugMenu.addAction(launchMadxAction2)
                def calcModelMatrixMenu():
                    calcModelMatrixAction = QAction('3) Calculate MAD-X model R matrix', self)
                    # lambda construction is needed to pass the parameter to the slot
                    calcModelMatrixAction.triggered.connect(lambda: self.calc.calcMatrRmodel(config.calcMethod))
                    # calcModelMatrixAction.triggered.connect(self.calc.calcMatrRmodel)
                    debugMenu.addAction(calcModelMatrixAction)
                def measureExpMatrixMenu():
                    measureExpMatrixAction = QAction('4) Measure experimental R matrix', self)
                    # lambda construction is needed to pass the parameter to the slot
                    measureExpMatrixAction.triggered.connect(lambda: self.calc.measureMatrRexp(config.calcMethod))
                    # measureExpMatrixAction.triggered.connect(self.calc.measureMatrRexp)
                    debugMenu.addAction(measureExpMatrixAction)     
                def calcMatricesDifferenceMenu():
                    calcMatricesDifferenceAction = QAction('5) Calculate difference matrix', self)
                    calcMatricesDifferenceAction.triggered.connect(self.calc.calcMatrRdiffNoArgs)
                    debugMenu.addAction(calcMatricesDifferenceAction)    
                def calcJMatrixMenu():   
                    calcJMatrixAction = QAction('6) Calculate J matrix', self)
                    calcJMatrixAction.triggered.connect(self.calc.calcJmatrNoArgs)
                    debugMenu.addAction(calcJMatrixAction)
                def findQuadErrorsMenu():
                    findQuadErrorsAction = QAction('7) Find quadrupole errors', self)
                    findQuadErrorsAction.triggered.connect(self.calc.findQuadErrors)
                    debugMenu.addAction(findQuadErrorsAction)
                
                deleteNewFilesMenu()
                launchMadxMenu2()
                calcModelMatrixMenu()
                measureExpMatrixMenu()
                calcMatricesDifferenceMenu()
                calcJMatrixMenu()
                findQuadErrorsMenu()
                
            fileMenu()
            calcMenu()
            helpMenu()
            if self.debug: setupDebugModeMenu()
            

            

        def setupInfoWindow():
            self.groupBox = QGroupBox("Lattice parameters")
            # self.groupBox.setStyleSheet('background-color: red')
            # self.groupBox.setEnabled(False)
            #===================================================================
            # p = self.groupBox.palette()
            # p.setColor(QPalette.Dark, Qt.red)
            # self.groupBox.setPalette(p)
            #===================================================================
            
            self.groupBox.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Expanding)  # horizontal and vertical
            self.groupBox.setMinimumWidth(200)  # Set fixed width
            # groupBox.setFixedWidth(300)
            # groupBox.setStyleSheet('font-size: 10pt')
            
            layoutInfo = QGridLayout()
            
            # layoutInfo.setAlignment(Qt.AlignRight)
            
            self.lblQuad = QLabel('Quadrupoles number', self)
            self.lblQuadNum = QLabel('nan', self)
            self.lblSext = QLabel('Sextupoles number', self)
            self.lblSextNum = QLabel('nan', self)
            self.lblMon = QLabel('Monitor number', self)
            self.lblMonNum = QLabel('nan', self)
            self.lblKick = QLabel('Kicker number', self)
            self.lblKickNum = QLabel('nan', self)
            
            self.lblTuneX = QLabel("Tune, Qx", self)
            self.lblTuneXVal = QLabel('nan', self)
            self.lblTuneY = QLabel("Tune, Qy", self)
            self.lblTuneYVal = QLabel('nan', self)
            self.lblChromX = QLabel("Chromaticity, hx", self)
            self.lblChromXVal = QLabel("nan", self)
            self.lblChromY = QLabel("Chromaticity, hy", self)
            self.lblChromYVal = QLabel("nan", self)
                        
            self.lblBetaXMax = QLabel("BetaX-Max [m]", self)
            self.lblBetaXMaxVal = QLabel('nan', self)
            self.lblBetaYMax = QLabel("BetaY-Max [m]", self)
            self.lblBetaYMaxVal = QLabel('nan', self)
            
            self.lblLength = QLabel("Length [m]", self)
            self.lblLengthVal = QLabel("nan", self)
            
            layoutInfo.addWidget(self.lblQuad, 0, 0)
            layoutInfo.addWidget(self.lblQuadNum, 0, 1)
            layoutInfo.addWidget(self.lblSext, 1, 0)
            layoutInfo.addWidget(self.lblSextNum, 1, 1)
            layoutInfo.addWidget(self.lblMon, 2, 0)
            layoutInfo.addWidget(self.lblMonNum, 2, 1)
            layoutInfo.addWidget(self.lblKick, 3, 0)
            layoutInfo.addWidget(self.lblKickNum, 3, 1)
            
            layoutInfo.addWidget(self.lblTuneX, 4, 0)
            layoutInfo.addWidget(self.lblTuneXVal, 4, 1)
            layoutInfo.addWidget(self.lblTuneY, 5, 0)
            layoutInfo.addWidget(self.lblTuneYVal, 5, 1)            
            layoutInfo.addWidget(self.lblChromX, 6, 0)
            layoutInfo.addWidget(self.lblChromXVal, 6, 1)         
            layoutInfo.addWidget(self.lblChromY, 7, 0)
            layoutInfo.addWidget(self.lblChromYVal, 7, 1)            
            layoutInfo.addWidget(self.lblBetaXMax, 8, 0)
            layoutInfo.addWidget(self.lblBetaXMaxVal, 8, 1)
            layoutInfo.addWidget(self.lblBetaYMax, 9, 0)
            layoutInfo.addWidget(self.lblBetaYMaxVal, 9, 1)
            
            layoutInfo.addWidget(self.lblLength, 10, 0)
            layoutInfo.addWidget(self.lblLengthVal, 10, 1)
            

            self.magnetType = QLabel('Magnet type')
            self.magnetTypeVal = QLabel('Click magnet')
            layoutInfo.addWidget(self.magnetType, 11, 0)
            layoutInfo.addWidget(self.magnetTypeVal, 11, 1)
            
            self.magnetName = QLabel('Magnet name')
            self.magnetNameVal = QLabel('Click magnet')
            layoutInfo.addWidget(self.magnetName, 12, 0)
            layoutInfo.addWidget(self.magnetNameVal, 12, 1)
            
            # magnRects = self.plotTws.magnetRectangles
            # print([rect.get_height() for rect in magnRects])   
                      
            
            
            spacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)  # stretch in the bottom part of the layout
            layoutInfo.addItem(spacer)

            layoutInfo.setHorizontalSpacing(20)  # Additional spacing between columns
            layoutInfo.setVerticalSpacing(10)  # Additional spacing between rows
            # layoutInfo.SetFixedSize(1000, 1000)
            
            self.groupBox.setLayout(layoutInfo)
            # self.centralLayout.addWidget(self.groupBox)
            # self.layout.addWidget(self.groupBox, 0, 1)
 
        # not yet included in the main window

        #=======================================================================
        # def setupButtons():
        #     # Signals and slots
        #     self.btnOpenFile = QPushButton("Open Twiss File")
        #     self.btnOpenFile.clicked.connect(self.openDialog)
        #     #self.btnPlotTwiss.clicked.connect(self.plotTwiss)
        #     self.btnLaunchMadx = QPushButton("Launch MAD-X")
        #     self.btnLaunchMadx.clicked.connect(self.calc.launchMadx)
        #     self.btnCalcMatrRmodel.clicked.connect(self.calcMatrRmodel)
        #     self.btnCalcMatrRexp.clicked.connect(self.measureMatrRexp)
        #     self.btnCalcMatrRdiff.clicked.connect(self.calcMatrRdiffNoArgs)
        #     self.btnCalcJmatr.clicked.connect(self.calcJmatrNoArgs)
        #     self.btnJmatrSVD.clicked.connect(self.findQuadErrors)
        #     self.btnDeleteNewFiles = QPushButton("Delete New Files")
        #     self.btnDeleteNewFiles.clicked.connect(self.fileManager.deleteNewFiles)
        #     self.btnRunORM = QPushButton("Run ORM analysis")
        #     self.btnRunORM.clicked.connect(self.calc.runORM)
        #     self.btnRunORM.setShortcut('Return')
        #=======================================================================
            
        def setupTabs():
            # self.tabs.setStyleSheet('background-color: #f0f0f0')
            def twissTab():
                twissWidget = QWidget()
                twissLayout = QHBoxLayout()
                
                twissLayout.addWidget(self.canvas)
                twissLayout.addWidget(self.groupBox)
            
                
                twissWidget.setLayout(twissLayout)
                
                self.opticFuncTabName = 'Optical functions'
                self.tabs.addTab(twissWidget, self.opticFuncTabName)
                
            def orbitTab():
                pass
#                 orbitWidget = QWidget()
#                 orbitLayout = QHBoxLayout()
                
                
            twissTab()
            orbitTab()
            
            #===================================================================
            # #self.tabs = calcResultsTab()
            # self.tabs = QTabWidget()
            # self.tabs.setTabsClosable(True)
            # self.tabs.tabCloseRequested.connect(self.closeTab)
            # #tabs.setMaximumHeight(10)
            # #tab1 = QWidget()
            # #button1 = QPushButton('Click me!')
            # self.tabs.insertTab(0, self.canvas, 'Optical functions')
            # print(self.tabs.currentIndex())
            # #self.tabs.insertTab(0, self.canvas, 'Optical functions')
            # 
            # #self.tabs.addTab(button1, 'Button')
            # #self.tabs.insertTab(0, button1, 'asdf')
            # 
            # #self.layout.addWidget(self.tabs, 0, 0, 2, 0)
            # self.layout.addWidget(self.tabs, 0, 0)
            #===================================================================

        #=======================================================================
        # def setupStartMessage():
        #     QMessageBox.about(self, "Message Box", 'I expect you to have the' + \
        #                       ' lattice file "twiss.txt" in the current directory')
        #=======================================================================
        
        def fillCentralLayout():
            # self.tabs.setAttribute(Qt.WA_TranslucentBackground)
            self.centralLayout.addWidget(self.tabs)
            # self.tabs.setStyleSheet('QTabWidget::pane {border-top: 10 px solid #f0f0f0; background-color: red}') # works
            # self.tabs.setStyleSheet('QTabWidget:tab-bar {left: 5 px; background-color: red}')
            # self.tabs.setStyleSheet('QTabWidget:tab {background-color: red}')
            # self.tabs.setStyleSheet('QTabWidget::pane {border-top: 5 px red} QTabBar::tab {background: green} ')
            
            # self.tabs.setStyleSheet('QTabWidget::pane {color: red;} QTabBar::tab {background-color: red} QTabWidget::tab-bar {background-color: blue}')
            # self.tabs.setStyleSheet('QTabWidget::pane {background-color: blue;} QTabBar::tab {background-color: red;}')
            # self.tabs.setStyleSheet('QTabBar::tab {background-color: #f0f0f0;}')
            # self.tabs.setStyleSheet('background-color: #f0f0f0')
            # self.tabs.tabBar().setStyleSheet('background-color: red')
            # self.tabs.setAutoFillBackground(False)
            # self.centralLayout.addWidget(self.canvas)
            # self.centralLayout.addWidget(self.groupBox)
            # color = self.groupBox.palette().color(QPalette.Background)
            # print(color.name())

# TODO: centralWidget should =  self.tabs, then add tab, in which in layout: canvas + info window
        setupStartupWindow()
        setupWindow()
        setupMenuBar()
        setupCentralWidget()
        setupPlotTwiss()
        setupInfoWindow()
        setupTabs()
        
        fillCentralLayout()
        
        # setupWindowLayoutWidgets()
        
        # setupButtons()
#         if STARTMESSAGE: setupStartMessage()

# TODO: Maybe think on introducing smth like a window.refresh.
    

    def showInfo(self):
        self.setLabelText(self.lblQuadNum, self.readTwiss.latticeElements['quad'])
        self.setLabelText(self.lblSextNum, self.readTwiss.latticeElements['sext'])
        self.setLabelText(self.lblKickNum, self.readTwiss.latticeElements['kick'])
        self.setLabelText(self.lblMonNum, self.readTwiss.latticeElements['mon'])

        self.setLabelText(self.lblTuneXVal, self.readTwiss.latInfo['Q1'])
        self.setLabelText(self.lblTuneYVal, self.readTwiss.latInfo['Q2'])
        self.setLabelText(self.lblBetaXMaxVal, self.readTwiss.latInfo['BETXMAX'])
        self.setLabelText(self.lblBetaYMaxVal, self.readTwiss.latInfo['BETYMAX'])
        self.setLabelText(self.lblChromXVal, self.readTwiss.latInfo['DQ1'])
        self.setLabelText(self.lblChromYVal, self.readTwiss.latInfo['DQ2'])
        self.setLabelText(self.lblLengthVal, self.readTwiss.latInfo['LENGTH'])
        
        return self.readTwiss.latticeElements, self.readTwiss.latInfo
        
        
    def GettingStartedInfo(self):
        fname = "help.txt"
        with open(fname, 'r') as fin:
            helpText = fin.read()
        # QMessageBox.about(self, self.tr("Getting started with Lattice engine"), self.tr(helpText))
        
        self.info = infoWindow(helpText)
        self.info.show()
        
        #=======================================================================
        # helpWindow = QTextEdit()
        # helpWindow.setReadOnly(True)
        # helpWindow.setText(helpText)
        # helpWindow.setAttribute(Qt.WA_DeleteOnClose)
        # helpWindow.exec_()
        #=======================================================================
        
    def setLabelText(self, label, text):
        label.setText(str(text))
      
    def statusBarMessage(self, message):
        self.statusBar().showMessage(message)

    def openDialog(self):
        fname = QFileDialog.getOpenFileName(self, 'Open file')[0]
        if fname:  # if the file was chosen
            self.calc.data, self.calc.latInfo = self.readTwiss.readTwissFile(fname)
            self.showInfo()
            self.plotTws.draw(self.canvas, self.figure, self.calc.data['S'],
                        self.calc.data['BETX'], self.calc.data['BETY'], self.calc.data['DX'])

        self.statusBarMessage("Plotting " + fname)
        
    def saveFigureDialog(self):
        fileName = QFileDialog.getSaveFileName(self, 'Save file', os.getcwd(), 'Images (*.pdf)')[0]
        
        print(self.tabs.tabText(self.tabs.currentIndex()))
        # if twiss
        #=======================================================================
        # if self.tabs.currentIndex() == 0:
        #     self.canvas.print_figure(fileName)
        # # if closed orbit
        # elif self.tabs.currentIndex() != 0:
        #     self.orbitCanvas.print_figure(fileName)
        #=======================================================================
        
        if self.tabs.tabText(self.tabs.currentIndex()) == self.opticFuncTabName:
            self.canvas.print_figure(fileName)
            
        try:
            if self.tabs.tabText(self.tabs.currentIndex()) == self.closOrbTabName:
                self.orbitCanvas.print_figure(fileName)
        except:
            pass
        
        try:
            if self.tabs.tabText(self.tabs.currentIndex()) == self.kickTabName:
                    self.kickCanvas.print_figure(fileName)
        except:
            pass
        
        try:
            if self.tabs.tabText(self.tabs.currentIndex()) == self.ormMatrVisTabName:
                self.ormMatrVisualCanvas.print_figure(fileName)
        except:
            pass

        
            
        
        # self.canvas.print_figure(fileName)
        # self.myFigSave.draw()
        # plt.draw()
        # self.myFigSave.savefig(fileName)
        
    def closeWindow(self):
        QApplication.quit()
        # self.close()
        
    def closeEvent(self, event):
        self.madxExec.deleteNewFiles()
        try:
            # Close infoWindow window if it is there
            self.coInfoWindow.close()
            # Close info window
        except:
            # We do not need an error that the child window does not exit. Just pass.
            pass

        try:
            # Close calcWindow window if it is there
            self.calcWindow.close()
            # Close calc window
        except:
            # We do not need an error that the child window does not exit. Just pass.
            pass
        
        try:
            self.startupWindow.close()
        except:
            pass

        
    def createCalcWindow(self):
        self.calcWindow = calcResultsWindow(self.thread)
        # calcWindow.exec_()
        self.calcWindow.show()
        
    def createCalcTab(self):
        idx = self.tabs.count()
        # infoButton = QPushButton('click me')
        # infoTextScreen = QTextEdit("In the process...<br>Wait for the results...")
        infoTextScreen = calcResultsTabInfo(self.thread)
        self.tabs.insertTab(idx, infoTextScreen, 'Calculation information')
        # Select the tab with information
        self.tabs.setCurrentIndex(idx)
        # infoButton.deleteLater()
        #=======================================================================
        # objects = self.findChildren(QObject)
        # print(len(objects))
        #=======================================================================
        
    def closeTab(self, currentIndex):
        widget = self.tabs.widget(currentIndex)
        # We do not close the first tab with canvas
        # print(self.tabs.currentIndex())
        if currentIndex != 0:
            if widget is not None:
                widget.deleteLater()
            self.tabs.removeTab(currentIndex)
            
            # print(self.tabs.currentWidget(), self.tabs.currentIndex())
            # self.tabs.currentWidget().deleteLater()

# TODO: maybe think on how to make one class instead of CalcResultsWindow and InfoWindow
        
    def closedOrbitInfo(self):
        # closOrb = closedOrbit()
        
        # TODO: So far the program hangs when computing many closed orbit. Do the QThread for that
        
        orbitFigure = plt.Figure(facecolor=self.figureBackground)
        orbitCanvas = FigureCanvas(orbitFigure)  # connect "figure" to "canvas"
        
        closOrb = closedOrbitWindow(orbitCanvas, orbitFigure)
        text = closOrb.correctionText
 
        self.orbitCanvas = closOrb.plotOrbit()
          
        # print(text)
        if DESIGN == 'windows':
            self.coInfoWindow = infoWindow(text)
            self.coInfoWindow.show()
        elif DESIGN == 'tabs':
              
            coInfoWindow = infoTab(text)
              
            orbitLayout = QHBoxLayout()
            orbitLayout.addWidget(self.orbitCanvas)
            orbitLayout.addWidget(coInfoWindow)
              
            orbitWidget = QWidget()
            orbitWidget.setLayout(orbitLayout)
              
            idx = self.tabs.count()
            self.closOrbTabName = 'Closed orbit'
            self.tabs.insertTab(idx, orbitWidget, self.closOrbTabName)
            # self.tabs.insertTab(idx, coInfoWindow, 'Closed orbit correction information')
            # Make the tab active
            self.tabs.setCurrentIndex(idx)
            
    def closedOrbitKickStrengths(self):
        orbitFigure = plt.Figure()
        orbitCanvas = FigureCanvas(orbitFigure)  # connect "figure" to "canvas"
        
        # figure and canvas come from the closedOrbitInfo()
        plotKickStren = KickersStrengthPlotWindow(orbitCanvas, orbitFigure)
        self.kickCanvas = plotKickStren.plotHisto()
        
        kickWidget = orbitCanvas  # just renaming
        
        idx = self.tabs.count()
        self.kickTabName = 'Kickers strength'
        self.tabs.insertTab(idx, kickWidget, self.kickTabName)
        # self.tabs.insertTab(idx, coInfoWindow, 'Closed orbit correction information')
        # self.tabs.setCurrentIndex(idx)
        
    def ORMMatrVisualize(self):
        ormMatrVisualFigure = plt.Figure()
        self.ormMatrVisualCanvas = FigureCanvas(ormMatrVisualFigure)  # connect "figure" to "canvas"
        
        ormMatrVisual = histo3dWindow(self.ormMatrVisualCanvas, ormMatrVisualFigure)
        
        def addSubplot(matr, currentPlot, totalPlots):
            ormMatrVisual.plotHisto(matr, currentPlot, totalPlots)
            
        #=======================================================================
        # def addSubplot(text):
        #     print("addSubplot")
        #     print(text)
        #=======================================================================
        
        # matr is emitted by visualORMMatrSignal and catched by addSubplot
        self.thread.calc.visualORMMatrSignal.connect(addSubplot)
        
        ormMatrVisWidget = self.ormMatrVisualCanvas  # just renaming
        idx = self.tabs.count()
        self.ormMatrVisTabName = 'ORM Matrix'
        self.tabs.insertTab(idx, ormMatrVisWidget, self.ormMatrVisTabName)
        
        
    def setWindowGeometry(self, offsetPxl, desktopRatio):
        desktopGeom = QApplication.desktop().availableGeometry()
        w, h = desktopGeom.width(), desktopGeom.height()
        wNew, hNew = w * desktopRatio, h * desktopRatio
        self.setGeometry(offsetPxl, offsetPxl, wNew - offsetPxl, hNew - offsetPxl)        
        
