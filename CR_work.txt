beam, particle = proton, energy = 3;
use, period=CRring;
select, flag=twiss, column=name, keyword, s, l, k1l, alfx, betx, mux, dx, alfy, bety, muy, dy, x, y;
TWISS,save,centre,file=twiss.txt;

return;