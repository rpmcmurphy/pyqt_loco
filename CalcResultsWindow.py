'''
Created on 8 Oct 2015

@author: localadmin_okovalen
'''

from PyQt5.QtWidgets import QMainWindow, QTextEdit, QWidget
from PyQt5.QtCore import Qt
from PyQt5.QtCore import QThread
from PyQt5.QtWidgets import qApp
from CalcThread import calcThread
from Calculations import calculations
from PyQt5.QtWidgets import QShortcut

from PyQt5.QtWidgets import QTabWidget

class calcResultsWindow(QMainWindow):
    """ This is the window wich is connected with calculations(calc) object.
     It shows the results of the calculations on the fly"""
    def __init__(self, myThread):
        super().__init__()
        self.setWindowTitle("ORM analysis results")
        self.setGeometry(100, 100, 300, 300)
        
        #self.shortcutExit = QShortcut(Qt.QKeySequence('Return'), self)
        #self.shortcutExit.activated.connect(self.close)
        
        
        self.setAttribute(Qt.WA_DeleteOnClose) # this will delete the object when it's closed
        self.infoTextScreen = QTextEdit("The ORM analysis is carried out...<br>Wait for the results...")
        self.infoTextScreen.setReadOnly(True)
        #text = calcThread.resultORMDump
        
        #calc = calculations()
        #calc.updatedCalcSignal.connect(self.setText)
        #self.infoTextScreen.setText(text)
        self.setCentralWidget(self.infoTextScreen)
        myThread.calc.updatedCalcSignal.connect(self.setText)
        
    def setText(self, text):
        self.infoTextScreen.setText(text)
        qApp.processEvents() # this triggers the emit-catch signals
        
        
#===============================================================================
# class calcResultsTab(QTabWidget):
#     def __init__(self, myThread):
#         super().__init__()
#         self.tabCloseRequested.connect(self.closeTab)
#         
#     def closeTab(self, currentIndex):
#         widget = self.tabs.widget(currentIndex)
#         if currentIndex != 0:
#             if widget is not None:
#                 widget.deleteLater()
#             self.tabs.removeTab(currentIndex)
#===============================================================================

class calcResultsTabInfo(QTextEdit):
    """ The information is in tab instead a window like in previous window class"""
    def __init__(self, myThread):
        super().__init__()
        self.setText("The Orbit Response Matrix analysis is carried out...<br>Wait for the results...")
        self.setAttribute(Qt.WA_DeleteOnClose)
        self.setReadOnly(True)
        
        myThread.calc.updatedCalcSignal.connect(self.setInfoText)
        
    def setInfoText(self, text):
        self.setText(text)
        qApp.processEvents()
        
    
        
