'''
Created on 8 Oct 2015

@author: localadmin_okovalen
'''

import matplotlib
#matplotlib.use("Qt5Agg",force=True)
#from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
import matplotlib.pyplot as plt
# from matplotlib import gridspec
from matplotlib import patches
import config

from drawMagnets import DrawMagnets

class plotTwiss(object):

    def __init__(self):
        """ self.magnetRectangles are needed for plotting the aperture.
        It is obtained from the DrawMagnets class. It will be passed into
        the MainWindow to define the tooltips: on hovering the mouse over
        the area of the certain magnet, its name will appear."""
        self.magnetRectangles = []
        self.magnetTypes = []
        self.magnetNames = []
        self.magnBotLeftX = []
        self.magnBotLeftY = []
        
    def fontSize(self, fontsize = 22):
        font = {'size'   : fontsize}
        matplotlib.rc('font', **font)
        matplotlib.rcParams['axes.labelsize'] = fontsize

    def draw(self, canvas, figure, s, betx, bety, dx):
        # Define the aspect ratio between subplots: aspRat:1
        aspRat = 5 #
        
        def drawTwiss():
#             self.fontSize()
            #myFigSave = None # This is to save the figure later
            
            def clearAxes():
                figure.clf() # remove the figure. Then I can plot another one
            clearAxes()  # so we do not have the plots overlap each other
                        # each time we add a new one
            #self.statusBarMessage("I'm in PlotTwiss")
            
            if config.plotMagnetsFlag:
                ax = figure.add_subplot(aspRat, 1, (2, aspRat))
            else:
                ax = figure.add_subplot(111)
            ax1 = ax.twinx()
            
            lw = 2
            #ax.plot(self.calc.data['S'], self.calc.data['BETX'], 'r', label="Beta-x", lw=lw)
            #ax.plot(self.calc.data['S'], self.calc.data['BETY'], 'b', label="Beta-y", lw=lw)
            ax.plot(s, betx, 'r', label="beta-x", lw=lw)
            ax.plot(s, bety, 'b', label="beta-y", lw=lw)
            ax.set_xlim(0, s.max() + 10)
            ax.legend(loc='upper left')
            ax.set_xlabel("path length [m]")
            ax.set_ylabel("Beta-x, Beta-y [m]")
            #ax1.plot(self.calc.data['S'], self.calc.data['DX'], 'g', label="Dx", lw=lw)
            ax1.plot(s, dx, 'g', label="Dx", lw=lw)
            ax1.legend(loc='upper right')
            ax1.set_ylabel("Dispersion Dx [m]")
            ax.grid(False)
            
            
            return ax
        
        # Plot Magnets
        def drawMagnets(ax):
            """ As an argument it accepts the axes of the twiss plot drawn earlier"""
            axMagnets = figure.add_subplot(aspRat,1,1, sharex = ax)
    
            axMagnets.get_xaxis().set_visible(False)
            axMagnets.get_yaxis().set_visible(False)
            
            drawMagnets = DrawMagnets()
            self.magnetRectangles, self.magnetTypes, self.magnetNames  = drawMagnets.defineRectangles()
    
            # These will be needed for tooltip in MainWindow
            self.magnBotLeftX = [x.get_x() for x in self.magnetRectangles]
            self.magnBotLeftY = [y.get_y() for y in self.magnetRectangles]
            
            for patch in self.magnetRectangles:
                axMagnets.add_patch(patch)
            axMagnets.set_label
    
            magnetPatches = []
            dipPatch = patches.Patch(color='cyan', label='D')
            magnetPatches.append(dipPatch)
            qfPatch = patches.Patch(color='red', label='QF')
            magnetPatches.append(qfPatch)
            qdPatch = patches.Patch(color='blue', label='QD')
            magnetPatches.append(qdPatch)
            sexPatch = patches.Patch(color='magenta', label='S')
            magnetPatches.append(sexPatch)
            kickPatch = patches.Patch(color='black', label='K')
            magnetPatches.append(kickPatch)
            monPatch = patches.Patch(color='green', label='M')
            magnetPatches.append(monPatch)        
            #TODO: the magnet name should be shown on the mouse click
            
            
    
            axMagnets.legend(handles = magnetPatches, ncol = 6, prop={'size': 8})
            
    #         print([[rect.get_width(), rect.get_height()] for rect in magnetRectangles])
    #         print([[rect.get_width(), rect.get_height()] for rect in magnetRectangles])
    #         print([rect.get_width() for rect in magnetRectangles])
            maxPlotMagnetAper = max([rect.get_height() for rect in self.magnetRectangles])
            axMagnets.set_ylim(-0.6*maxPlotMagnetAper, 0.6*maxPlotMagnetAper)


        ax = drawTwiss()
        if config.plotMagnetsFlag:
            drawMagnets(ax)
            
        figure.tight_layout() # Reduce slightly the grey margins around the plot
#        figure.subplots_adjust(left=0.05, right=0.95, top=0.95, bottom=0.05)
        
        canvas.draw()

        
        return canvas
        
        
        
    
        