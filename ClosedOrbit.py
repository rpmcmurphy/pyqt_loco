'''
We create the new file where we put the 
 
 
Created on Oct 16, 2015
 
@author: RP
'''
import numpy as np
import io
 
from MadxExecution import madxExecution
 
import matplotlib
matplotlib.use("Qt5Agg", force=True)
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
from collections import OrderedDict
 
import config
 
COMMENT_ALL = False
 
def defineMadxCount(iterations):
    if iterations == 1:
        madxCount = str(5)  # seed for one iteration
    elif iterations > 1:
        madxCount = 'count'
    return madxCount
 
class magnetErrors:
    def __init__(self, dipErrorFile, quadErrorFile):
        self.dipPattern = 'cr..mh'
        self.quadPattern = 'cr..qs'
        self.sextPattern = 'cr..ks'
        self.kickPattern = '^ki'
        self.bpmPattern = 'bpm*'
        
        self.dipErrorFile = dipErrorFile
        self.quadErrorFile = quadErrorFile

            
        
        self.errorTextData = ''

        self.distrib = 'ranf'  # Can also be 'tgauss'
        # self.distrib = 'tgauss'
        
        # self.sigma = str(1.0E-3) # mm
        
        # sigma value for misalignments for dipoles and quads
        
        # self.sigma is for:
        # a) Sextupoles misalignments and rolls
        # b) BPM misalignments, rolls and reading errors.
        self.sigma = str(0.5E-3)
        # self.bpmAbsoluteSigma = str(0.5E-3)
        # self.bpmScaleSigma = str(0.05)
        self.quadMisalignSigma = str(1.0E-3)  # sigma for the quadrupole misalignments
        self.quadRollSigma = str(0.5E-3)
        self.dipMisalignSigma = str(2.0E-3)  # sigma for the dipole misalignments
        self.dipRollSigma = str(0.5E-3)
        
        # cutoff for the gauss distribution
        if self.distrib == 'tgauss':
            self.sigmaCutoff = str(2)
         
        self.iterations = config.closOrbIterations
        self.madxCount = defineMadxCount(self.iterations)
         
        magnets = self.extractMagnetPattern(config.first)
        self.dipoles, self.quadrupoles = magnets['dipoles'], magnets['quadrupoles']
        self.count = 0  # for assigning the error seed
 
 
 
    def extractMagnetPattern(self, fileName):
        ''' If the magnet is split in e.g. halves, the closed orbit error module
        adds different errors for these halves even though the names are the same.
         So we need to find the quadrupoles in order to extract the pattern and
         assign the same errors to the parts of the same magnet
         magnetType is either 'quadrupoles' or 'sbend'
         '''
        # all quadrupoles: among them there are those with zero strength - 
        # they are basically drifts
        quads = []
        realQuads = []  # the quadrupoles with nonzero strength
        dipoles = []
        with open(config.first, 'r') as f:
            data = f.readlines()
            for line in data:
                if 'quadrupole' in line.lower():
                    quadName = line.split(':')[0]
                    quads.append(quadName)
                    if quadName[0:2] == 'cr' and quadName[4:6] == 'qs':
                        realQuads.append(quadName)
                elif 'sbend' in line.lower():
                    dipName = line.split(':')[0]
                    dipoles.append(dipName)
        # print('dipoles', dipoles)
        magnets = OrderedDict({'dipoles': dipoles, 'quadrupoles': realQuads})
        return magnets
     
    def clearErrorClassMagnet(self):
        self.errorTextData += 'select, flag = error, clear;\n'
     
    def addErrorClassMagnet(self, pattern=''):
        # text += 'Select, flag=ERROR, class=MQ;'
        self.errorTextData += 'select, flag = error, pattern = "' + pattern + '";\n'
         
    def addEoption(self, addFlag, seed=None):
        if seed == None:
            self.errorTextData += 'eoption, add = ' + addFlag + ';\n'
        elif seed != None:
            self.errorTextData += 'eoption, add = ' + addFlag + ', seed = ' + seed + ';\n'
            
    def multipoleErrors(self, pattern, order, radius, errNorm, errSkew):
        self.errorTextData += 'efcomp, order = ' + order + ', radius = ' + radius + ',\n' + \
                            'dknr = {'
        # adding normal errors
        for val in errNorm[0:-1]:
            self.errorTextData += str(val) + ', '
            # self.errorTextData += ', '
        self.errorTextData += errNorm[-1] + '},\n'
        # adding skew errors
        self.errorTextData += 'dksr = {'
        for val in errSkew[0:-1]:
            self.errorTextData += str(val) + ', '
        self.errorTextData += errSkew[-1] + '};\n'
     
    def misalignmentsAndRolls(self, misalignSigma, rollSigma, flag=':'):
        if self.distrib == 'tgauss':
            self.errorTextData += 'ealign, ' + \
             'dx' + flag + '= ' + self.distrib + '(' + self.sigmaCutoff + ')*' + misalignSigma + ', ' + \
             'dy' + flag + '= ' + self.distrib + '(' + self.sigmaCutoff + ')*' + misalignSigma + ', ' + \
             'ds' + flag + '= ' + self.distrib + '(' + self.sigmaCutoff + ')*' + misalignSigma + ', ' + \
             'dphi' + flag + '= ' + self.distrib + '(' + self.sigmaCutoff + ')*' + rollSigma + ', ' + \
             'dtheta' + flag + '= ' + self.distrib + '(' + self.sigmaCutoff + ')*' + rollSigma + ', ' + \
             'dpsi' + flag + '= ' + self.distrib + '(' + self.sigmaCutoff + ')*' + rollSigma + ';\n'
        elif self.distrib == 'ranf':
            self.errorTextData += 'ealign, ' + \
             'dx' + flag + '= -' + misalignSigma + ' + ' + self.distrib + '()*' + '2*' + misalignSigma + ', ' + \
             'dy' + flag + '= -' + misalignSigma + ' + ' + self.distrib + '()*' + '2*' + misalignSigma + ', ' + \
             'ds' + flag + '= -' + misalignSigma + ' + ' + self.distrib + '()*' + '2*' + misalignSigma + ', ' + \
             'dphi' + flag + '= -' + rollSigma + ' + ' + self.distrib + '()*' + '2*' + rollSigma + ', ' + \
             'dtheta' + flag + '= -' + rollSigma + ' + ' + self.distrib + '()*' + '2*' + rollSigma + ', ' + \
             'dpsi' + flag + '= -' + rollSigma + ' + ' + self.distrib + '()*' + '2*' + rollSigma + ';\n'  
             
        #=======================================================================
        # self.errorTextData += 'ealign, ' + \
        #  'dy' + flag + '= ' + self.distrib + '()*0.5e-3, ' + \
        #  'dx' + flag + '= ' + self.distrib + '()*0.5e-3, ' + \
        #  'ds' + flag + '= ' + self.distrib + '()*0.5e-3, ' + \
        #  'dphi' + flag + '= ' + self.distrib + '()*0.5e-3, ' + \
        #  'dtheta' + flag + '= ' + self.distrib + '()*0.5e-3, ' + \
        #  'dpsi' + flag + '= ' + self.distrib + '()*0.5e-3;\n'
        #=======================================================================             

          
 
          
    def dipErrors(self, pattern=''):
        for dipole in self.dipoles:
            self.clearErrorClassMagnet()
            self.addErrorClassMagnet(dipole)  # misalign all magnetic elements
            # self.addEoption('False', '1000 * ' + self.madxCount + ' + ' + str(self.count))
            self.addEoption('False', self.madxCount + ' + ' + str(np.random.randint(0, 99999999)))
            self.misalignmentsAndRolls(self.dipMisalignSigma, self.dipRollSigma, '')
            sigma = str(3)
            self.multipoleErrors(pattern=dipole, order='0', radius='0.19',
                                 errNorm=['0',
                                            '0',
                                            '(63e-5)*(1+0.2*TGAUSS(' + sigma + '))',
                                            '(3e-6)*(1+0.2*TGAUSS(' + sigma + '))',
                                            '(17e-5)*(1+0.2*TGAUSS(' + sigma + '))',
                                            '(88e-8)*(1+0.2*TGAUSS(' + sigma + '))',
                                            '(-20e-8)*(1+0.2*TGAUSS(' + sigma + '))',
                                            '(-12e-8)*(1+0.2*TGAUSS(' + sigma + '))',
                                            '(10e-8)*(1+0.2*TGAUSS(' + sigma + '))',
                                            '(-70e-8)*(1+0.2*TGAUSS(' + sigma + '))',
                                            '0', '0', '0', '0', '0', '0', '0', '0', '0', '0',
                                            ],
                                 errSkew=['0', '0', '0', '0', '0', '0', '0', '0', '0', '0',
                                            '0', '0', '0', '0', '0', '0', '0', '0', '0', '0',
                                            ])
            self.count += 1
         
    def quadErrors(self, pattern=''):
        narrowQuads = ['cr01qs03', 'cr01qs04',
                       'cr02qs01', 'cr02qs02', 'cr02qs03', 'cr02qs04',
                       'cr03qs02', 'cr03qs03', 'cr03qs04',
                       'cr04qs03', 'cr04qs04']  
        for quad in self.quadrupoles:
            self.clearErrorClassMagnet()
            self.addErrorClassMagnet(pattern=quad)  # misalign all magnetic elements
            # self.addEoption('False', '1000 * ' + self.madxCount + ' + ' + str(self.count))
            self.addEoption('False', self.madxCount + ' + ' + str(np.random.randint(0, 99999999)))
            self.misalignmentsAndRolls(self.quadMisalignSigma, self.quadRollSigma, '')
            sigma = str(3)
            if quad in narrowQuads:
                # print('narrow', quad)
                self.multipoleErrors(pattern=quad, order='1', radius='0.09',
                                 errNorm=['0',
                                            '0',
                                            '0',
                                            '0',
                                            '0',
                                            '(620e-6)*(1+0.2*TGAUSS(' + sigma + '))',
                                            '0',
                                            '0',
                                            '0',
                                            '(381e-6)*(1+0.2*TGAUSS(' + sigma + '))',
                                            '0',
                                            '0',
                                            '0',
                                            '(65e-6)*(1+0.2*TGAUSS(' + sigma + '))',
                                            '0',
                                            '0',
                                            '0',
                                            '(4e-6)*(1+0.2*TGAUSS(' + sigma + '))',
                                            '0',
                                            '0',
                                            ],
                                 errSkew=['0', '0', '0', '0', '0', '0', '0', '0', '0', '0',
                                            '0', '0', '0', '0', '0', '0', '0', '0', '0', '0',
                                            ])
            else:
                # print('wide', quad)
                self.multipoleErrors(pattern=quad, order='1', radius='0.09',
                                 errNorm=['0',
                                            '0',
                                            '0',
                                            '(4e-8)*(1+0.2*TGAUSS(' + sigma + '))',
                                            '(1e-6)*(1+0.2*TGAUSS(' + sigma + '))',
                                            '(-3e-4)*(1+0.2*TGAUSS(' + sigma + '))',
                                            '(1e-6)*(1+0.2*TGAUSS(' + sigma + '))',
                                            '(2e-8)*(1+0.2*TGAUSS(' + sigma + '))',
                                            '(1e-6)*(1+0.2*TGAUSS(' + sigma + '))',
                                            '(10e-4)*(1+0.2*TGAUSS(' + sigma + '))',
                                            '0',
                                            '0',
                                            '0',
                                            '0',
                                            '0',
                                            '0',
                                            '0',
                                            '0',
                                            '0',
                                            '0',
                                            ],
                                 errSkew=['0', '0', '0', '0', '0', '0', '0', '0', '0', '0',
                                            '0', '0', '0', '0', '0', '0', '0', '0', '0', '0',
                                            ])
            
            self.count += 1
            # self.addEoption(False, seed)
            # self.errorTextData += 
        # addErrorClassMagnet(pattern = pattern) # misalign all magnetic elements
        
    def sextErrors(self, pattern=''):
        self.clearErrorClassMagnet()
        self.addErrorClassMagnet(pattern=pattern)
        self.addEoption('False')
        self.misalignmentsAndRolls(self.sigma, self.sigma, ':')
        sigma = str(3)
        self.multipoleErrors(pattern=pattern, order='2', radius='0.2',
                             errNorm=['0',
                                        '0',
                                        '0',
                                        '0',
                                        '0',
                                        '0',
                                        '0',
                                        '0',
                                        '(-0.69e-3)*(1+0.2*TGAUSS(' + sigma + '))',
                                        '0',
                                        '0',
                                        '0',
                                        '0',
                                        '0',
                                        '(-2.03e-3)*(1+0.2*TGAUSS(' + sigma + '))',
                                        '0', '0', '0', '0', '0',
                                        ],
                             errSkew=['0', '0', '0', '0', '0', '0',
                                        '(-0.4e-2)*(1+0.2*TGAUSS(' + sigma + '))',
                                        '0',
                                        '0',
                                        '0',
                                        '(1.232e-2)*(1+0.2*TGAUSS(' + sigma + '))',
                                        '0', '0', '0',
                                        '(-0.75e-2)*(1+0.2*TGAUSS(' + sigma + '))',
                                        '0', '0', '0', '0', '0',
                                        ])
         
    def kickErrors(self, pattern=''):
        self.clearErrorClassMagnet()
        self.addErrorClassMagnet(pattern=pattern)  # misalign all magnetic elements
        self.misalignmentsAndRolls(self.sigma, self.sigma, ':')
         
    def bpmErrors(self, pattern=''):
        def readErrors():
            self.addEoption('true')  # Do not overwrite the errors
            if self.distrib == 'tgauss':
                self.errorTextData += 'ealign, ' + \
                 'mrex:= ' + self.distrib + '(' + self.sigmaCutoff + ')*' + self.sigma + ',' + \
                 'mrey:= ' + self.distrib + '(' + self.sigmaCutoff + ')*' + self.sigma + ',' + \
                 'mscalx:= 0.05, mscaly: = 0.05;\n'
            elif self.distrib == 'ranf':
                self.errorTextData += 'ealign, ' + \
                 'mrex:= -' + self.sigma + ' + ' + self.distrib + '()*' + '2*' + self.sigma + ', ' + \
                 'mrey:= -' + self.sigma + ' + ' + self.distrib + '()*' + '2*' + self.sigma + ', ' + \
                 'mscalx:= 0.05, mscaly: = 0.05;\n'
            self.addEoption('false')
 
        self.clearErrorClassMagnet()
        self.addErrorClassMagnet(pattern=pattern)  # misalign all magnetic elements
        self.misalignmentsAndRolls(self.sigma, self.sigma, ':')
        readErrors()
        
    def saveErrors(self, pattern, file):
            self.clearErrorClassMagnet()
            self.errorTextData += 'select, flag = error, pattern = ' + pattern + ';\n'
            self.errorTextData += 'esave, file = ' + file + ';\n'

             
    def assignErrors(self):
        self.addEoption('false', str(self.madxCount))
        

        self.quadErrors(self.quadPattern)
        self.saveErrors(self.quadPattern, self.quadErrorFile)
        self.dipErrors(self.dipPattern)
        self.saveErrors(self.dipPattern, self.dipErrorFile)
        
        self.sextErrors(self.sextPattern)
        self.kickErrors(self.kickPattern)
        self.bpmErrors(self.bpmPattern)
        
        #=======================================================================
        # if self.plotErrorFlag:
        #      plotErrors = plotMagnetErrors(dipErrorFile, quadErrorFile)
        #      plotErrors.plotErrors()
        #=======================================================================

        
        return self.errorTextData
    
    
class plotMagnetErrors:
    def __init__(self, dipErrorFile, quadErrorFile):
        self.dipErrorFile = dipErrorFile
        self.quadErrorFile = quadErrorFile
        self.dipDict = dict()
        self.quadDict = dict()
        
    def readErrorFile(self, file):
        with open(file, 'r') as f:
            data = []
            for line in f:
                if line[0] == '@':
                    pass
                elif line[0] == '$':
                    pass
                elif line[0] == '*':
                    line = line[1:]
                    data.append(line)
                else:
                    data.append(line)
        # print('asdf')
        # print(data)
        stream = ''.join(data)
        
        dataBytes = stream.encode('utf-8')
        dataDict = np.genfromtxt(io.BytesIO(dataBytes), names=True, dtype=None)
        # print(dataDict.dtype.names)
        # print(dataDict['DPHI'])
        return dataDict
        
            
    def plotErrors(self):
        self.dipDict = self.readErrorFile(self.dipErrorFile)
        self.quadDict = self.readErrorFile(self.quadErrorFile)
        
        #=======================================================================
        # print(self.dipDict['DPHI'])
        # print(self.quadDict['DPHI'])
        #=======================================================================
        
        
        #=======================================================================
        # fig, (axD, axQ) = plt.subplots(2, 1)
        #  
        # dphiD = self.dipDict['DPHI']
        # x = np.arange(len(dphiD))
        # y = dphiD
        # axD.scatter(x, y)
        # axD.set_ylim(ymin = min(y), ymax = max(y))
        #  
        #  
        # dphiQ = self.quadDict['DPHI']
        # x = np.arange(len(dphiQ))
        # y = dphiQ
        # axQ.scatter(x, y)
        # axQ.set_ylim(ymin = min(y), ymax = max(y))
        #  
        # plt.plot()
        #=======================================================================
        
        #=======================================================================
        # fig, axD = plt.subplots(1, 1)     
        # dphiD = self.dipDict['DPHI']
        # x = np.arange(len(dphiD))
        # y = dphiD
        # axD.scatter(x, y)
        # axD.set_ylim(ymin = min(y), ymax = max(y))
        # 
        # fig, axQ = plt.subplots(1, 1)
        # dphiQ = self.quadDict['DPHI']
        # x = np.arange(len(dphiQ))
        # y = dphiQ
        # axQ.scatter(x, y)
        # axQ.set_ylim(ymin = min(y), ymax = max(y))
        # 
        # plt.plot
        #=======================================================================
        
        
        def drawPlot(dict, title):
            fig, axs = plt.subplots(2, 3)
            plt.suptitle(title)
            fig.canvas.set_window_title(title)
            
            def drawSubplot(dict, var, ax):
                dpsi = dict[var]
                x = np.arange(len(dpsi))
                y = dpsi
                ax.scatter(x, y, label=var)
                offset = 0.1
                ax.set_ylim(ymin=(1 + offset) * min(y), ymax=(1 + offset) * max(y))
                ax.set_xlim(xmin=-1)
                ax.legend()
                
            
            drawSubplot(dict, 'DX', axs[0, 0])
            drawSubplot(dict, 'DY', axs[0, 1])
            drawSubplot(dict, 'DS', axs[0, 2])
            
            drawSubplot(dict, 'DPHI', axs[1, 0])
            drawSubplot(dict, 'DTHETA', axs[1, 1])
            drawSubplot(dict, 'DPSI', axs[1, 2])
        
        
        drawPlot(self.dipDict, 'Dipoles misalignments and rolls')
        drawPlot(self.quadDict, 'Quadrupoles misalignments and rolls')
        
        
        plt.show()

        
  
     
 
class closedOrbit:
    def __init__(self, plotErrorFlag=False):
        self.plotErrorFlag = plotErrorFlag
        
        self.dipErrorFile = 'diperrors.txt'
        self.quadErrorFile = 'quaderrors.txt'
        
        self.textData = ''
         
        self.iterations = config.closOrbIterations
         
        self.magnetErrors1 = magnetErrors(self.dipErrorFile, self.quadErrorFile)
        self.magnetErrors2 = magnetErrors(self.dipErrorFile, self.quadErrorFile)
        self.iterations = config.closOrbIterations
        self.madxCount = defineMadxCount(self.iterations)
         
        self.madxExec = madxExecution()
 
         
        self.newCorrFile = 'closed_orbit_correction.txt'
         
        self.ending = '_cor'
        configFirstFileName, configFirstFileExt = config.first.split('.')  # @UndefinedVariable (not checked at runtime)
        self.newCallFile = configFirstFileName + self.ending + '.' + configFirstFileExt
         
        self.ring = 'CRring'
        self.modelOrbitTable = 'model_orbit'
        self.orbitUncorTable = 'uncor_orbit'
        self.orbitUncorFile = 'uncor_orbit.txt'
        self.orbitCorTable = 'cor_orbit'
        self.orbitCorFile = 'cor_orbit.txt'
         
        # These two files below should be the same
        self.errorFile1 = 'errors1.txt'
        self.errorFile2 = 'errors2.txt'
         
        # In case if there are many iterations
        # One file for the maximum deviations of the corrected and uncorrected orbits
        self.coOrbitMaxFile = 'comax_all.txt'
         
        self.orbitUncorData = []
         
        # self.textData = '<br>Closed orbit correction textData is below:<br>'
         
             
             
    # chunk of text for mad-x exection - closed orbit correction 
    def magnetType(self):
        """ Here the magnet class will be defined like MQ (quads), B (dipoles), so one.
        It is needed for applying the errors."""
        pass
     
    def addIterations(self):
        self.textData += 'iterations = ' + str(self.iterations) + ';\n'
        self.textData += self.madxCount + ' = 0;\n'
        self.textData += 'while (count < iterations) {\n'
         
    def addUsePeriod(self):
        ''' After USE command the errors are erased'''
        self.textData += 'use, period = ' + self.ring + ';\n'
         
    def addEndCycle(self):
        self.textData += self.madxCount + ' = ' + self.madxCount + ' + 1;\n'
        self.textData += '};\n'
     
    def saveTargetOrbitTable(self):
        self.textData += 'twiss, table = ' + self.modelOrbitTable + ',sequence = ' + self.ring + ';\n'
             
    def addEoption(self, addFlag, seed=None):
        if seed == None:
            self.textData += 'eoption, add = ' + addFlag + ';\n'
        elif seed != None:
            self.textData += 'eoption, add = ' + addFlag + ', seed = ' + seed + ';\n'
        # 'add' is needed to not add the errors each iteration
        # textData += text        
 
 
 
    def assignErrors(self, magnetErrors):
        self.textData += magnetErrors.assignErrors()
 
    def saveErrors(self, fileName=''):
        self.textData += 'select, flag = error, full;\n'
        self.textData += 'esave, file = ' + fileName + ';\n'
 
    def addTwissCommand(self):
        self.textData += 'select, flag = twiss, clear;\n'
        self.textData += 'select, flag = twiss, column=name, s, x;\n'
         
    def computeOrbit(self, table):
        self.textData += 'twiss, table = ' + table + ', sequence = ' + self.ring + ';\n'
     
    def saveOrbit(self, fileName):
        self.textData += 'twiss, save, file = ' + fileName + ';\n'
         
    def saveClosedOrbitMax(self, fromFileName, toFileName):
        """ Obsolete """
        self.textData += 'system, "more +27 ' + fromFileName + ' > tmp.txt"; // Delete first 27 lines\n'
        self.textData += 'system, "head -2 tmp.txt >> ' + toFileName + '"; // Delete all others except first 2 lines\n'
         
    def addCorrectionCommand(self):
        """ http://madx.web.cern.ch/madx/madX/doc/latexuguide/madxuguide.pdf
        COND: When COND=1, a Singular Value Decomposition is performed
        ERROR: specifies the maximum RMS value
        RESOUT: This command outputs the results for all monitors and all correctors in a computer
            readable format
        CORZERO: an integer value to specify whether corrector settings should be all reset to
            zero before starting the orbit correction (CORZERO> 0)
        CORRLIM: A limit on the maximum corrector strength can be given and a WARNING is
            issued if it is exceeded by one or more correctors. 
        MONON: = 0.95: 5% of bpms are faulty
        monerror: This is needed for mrex, mrey (abs bpm reading error) to work
        monscale: This is needed for mscalx, mscaly to work (rel bpm reading error)
         """
        self.textData += 'CORRECT, FLAG = ring, orbit = ' + self.orbitUncorTable + \
        ',\n\tmodel = ' + self.modelOrbitTable + ', MODE = svd, cond = 0, ERROR = 1E-6, PLANE = y,' + \
        '\n\tmlist = my.tab, clist = cy.tab, RESOUT = 1, corzero = 1, corrlim = 1.0, MONON = 1.0, monerror = 1.0, monscale = 1.0;\n'
 
        self.textData += 'CORRECT, FLAG = ring, orbit = ' + self.orbitUncorTable + \
        ',\n\tmodel = ' + self.modelOrbitTable + ', MODE = svd, cond = 0, ERROR = 1E-6, PLANE = x,' + \
        '\n\tmlist = mx.tab, clist = cx.tab, RESOUT = 1, corzero = 1, corrlim = 1.0, MONON = 1.0, monerror = 1.0, monscale = 1.0;\n'
          
         
    #===========================================================================
    # def plotOrbit(self):
    #     self.textData += 'plot, table=my_orbit, title="Uncorrected orbit x";\n'
    #===========================================================================
         
    def createDataTable(self, tableName, columns):
        ''' use it as createDataTable(comax_table, [xcomax, ycomax])'''
        columnsText = ''
        for i, value in enumerate(columns):
            if i == 0:
                columnsText += value
            else:
                columnsText += ', '
                columnsText += value  
        self.textData += 'create, table = ' + tableName + ', column = ' + columnsText + ';\n'
         
    def fillDataTable(self, tableName):
        self.textData += 'fill, table = ' + tableName + ';\n'
     
    def writeDataTable(self, tableName, fileName):
        self.textData += 'write, table = ' + tableName + ', file = ' + fileName + ';\n'
         
    def assignBtoA(self, a, b):
        self.textData += str(a) + ' = ' + str(b) + ';\n';
         
    def addReturn(self):
        self.textData += 'return;'        
                 
         
    def createCorrectionFile(self, fname, dataNew):
        open(fname, 'w').close()  # Clear file
        with open(fname, 'w') as f:
            f.writelines(dataNew)
             
    def createCallableFile(self, fname):
              
        def parseMainMadxFile():
            with open(config.first, 'r') as f:
                mainData = f.readlines()
            mainDataNew = mainData[:]
            for i, line in enumerate(mainData):
                if 'stop' in line.lower():
                    mainDataNew.insert(i - 1, 'call, file = "' + self.newCorrFile + '"\n')
            # print('\n'.join(mainDataNew))
            # print(mainDataNew)
            return mainDataNew
         
        # Actually create the file with the mainDataNew
        newMainData = parseMainMadxFile()
        with open(fname, 'w') as f:
            f.writelines(newMainData)
             
    def trimOrbitFile(self):
        skipFirstLines = 27
        skipLastLines = -2
        with open(self.orbitFile, 'r') as f:
            data = f.readlines()
        with open(self.orbitFile, 'w') as f:
            # print('\n'.join(textData[skipFirstLines : skipLastLines]))
            f.writelines(data[skipFirstLines : skipLastLines])
             
    def readUncorOrbitFile(self, fileName):
        data = np.loadtxt(fileName, skiprows=47, usecols=(1, 2))
        self.orbitUncorData = data.T
        # print(self.orbitUncorData)
         
    def readCorOrbitFile(self, fileName):
        data = np.loadtxt(fileName, skiprows=47, usecols=(1, 2))
        self.orbitCorData = data.T
        # print(self.orbitUncorData)
 
    def plotClosedOrbit(self):
        plt.plot(self.orbitUncorData[0], self.orbitUncorData[1])
        plt.show()
     
         
    def deleteCorrectionFiles(self, files):
        # self.madxExec.deleteFile(self.newCallFile)
        for file in files:
            self.madxExec.deleteFile(file)
                     
             
                 
    #===========================================================================
    # def saveClosedOrbitMax(self, fileName):
    #     with open(fileName, 'a'):
    #===========================================================================
             
     
     
    def clearFile(self, fileName):
        with open(fileName, 'w') as f:
            f.truncate()
     
     
    def correct(self):
        # self.readFile()
        # dataNew = self.correctionBody()
        # self.createNewFile(dataNew)
         
        if COMMENT_ALL: self.textData += '/*'
         
        # errors = magnetErrors(self.textData)
         
        if self.iterations == 1:
            np.random.seed(0)  # For errors
            self.saveTargetOrbitTable()
            # super().addEoption('false', str(self.seed))
            self.assignErrors(self.magnetErrors1)
            # self.addErrors()
            self.saveErrors(self.errorFile1)
            self.addTwissCommand()
            self.computeOrbit(self.orbitUncorTable)
            self.saveOrbit(self.orbitUncorFile)
            self.addCorrectionCommand()
             
            # Add the errors again after the correction
            np.random.seed(0)  # For errors
            # super().addEoption('false', str(self.seed))
            self.assignErrors(self.magnetErrors2)
            # self.addErrors()
            self.saveErrors(self.errorFile2)
            self.addTwissCommand()
            self.computeOrbit(self.orbitCorTable)
            self.saveOrbit(self.orbitCorFile)
             
             
            self.addReturn()
             
             
            self.createCorrectionFile(self.newCorrFile, self.textData)
            self.createCallableFile(self.newCallFile)
             
            newLaunch = 'Launch' + self.ending + '.bat'
            self.madxExec.createLaunchBat(newLaunch, self.newCallFile)
            self.madxExec.launchBat(newLaunch)
             
            self.readUncorOrbitFile(self.orbitUncorFile)
            self.readCorOrbitFile(self.orbitCorFile)

            
            
             
        elif self.iterations > 1:
            np.random.seed(0)  # For errors
            # Create the files to save the orbit each iteration
            uncorOrbitMaxFile = 'uncor_orbit_max.txt'
            corOrbitMaxFile = 'cor_orbit_max.txt'
             
            # Create one for all iterations table which will be filled the maximum 
            # orbit deviations after each iteration
            xUncorComax, yUncorComax = 'xuncorcomax', 'yuncorcomax'  # For the table
            xCorComax, yCorComax = 'xcorcomax', 'ycorcomax'  # For the table
            coMaxTable = 'comax_table'
            self.createDataTable(coMaxTable, [xUncorComax, yUncorComax, xCorComax, yCorComax])
            # Create the file for the table above
             
            # Clear the files before the start
            self.clearFile(uncorOrbitMaxFile)
            self.clearFile(corOrbitMaxFile)
             
            self.addIterations()  # Start of the cycle
            self.addUsePeriod()
            self.saveTargetOrbitTable()
            # self.addEoption('false', self.count)
            self.assignErrors(self.magnetErrors1)
            #  self.addErrors()
            self.saveErrors(self.errorFile1)
            self.addTwissCommand()
            self.computeOrbit(self.orbitUncorTable)
            self.saveOrbit(self.orbitUncorFile)
            self.assignBtoA(xUncorComax, 'table(summ, xcomax)')
            self.assignBtoA(yUncorComax, 'table(summ, ycomax)')
            # open file uncor_orbit_max.txt and cor_orbit_max.txt
            # self.saveClosedOrbitMax(self.orbitUncorFile, uncorOrbitMaxFile)
            self.addCorrectionCommand()
             
            # Add the errors again after the correction
            np.random.seed(0)  # For errors
            # self.addEoption('false', self.count)
            self.assignErrors(self.magnetErrors2)
            # self.addErrors()
            self.saveErrors(self.errorFile2)
            self.addTwissCommand()
            self.computeOrbit(self.orbitCorTable)
            self.saveOrbit(self.orbitCorFile)
            self.assignBtoA(xCorComax, 'table(summ, xcomax)')
            self.assignBtoA(yCorComax, 'table(summ, ycomax)')
            # self.saveClosedOrbitMax(self.orbitCorFile, corOrbitMaxFile)
            self.fillDataTable(coMaxTable)
            self.addEndCycle()  # End of the cycle
            # Finally write the table to the file
            self.writeDataTable(coMaxTable, self.coOrbitMaxFile) 
             
            # self.textData += 'xco = table(summ, xcomax);\nvalue, xco;\n'
             
            self.addReturn()
             
             
             
            self.createCorrectionFile(self.newCorrFile, self.textData)
            self.createCallableFile(self.newCallFile)
             
            newLaunch = 'Launch' + self.ending + '.bat'
            self.madxExec.createLaunchBat(newLaunch, self.newCallFile)
            self.madxExec.launchBat(newLaunch)
            
        if config.plotMagnetMisalignRoll:
            plotErrors = plotMagnetErrors(self.dipErrorFile, self.quadErrorFile)
            plotErrors.plotErrors()
             
        if COMMENT_ALL: self.textData += '*/'
        # self.plotClosedOrbit()
         
        return self.textData
     
 
         
         
class closedOrbitWindow(object):
    def __init__(self, canvas, figure):
        plotErrors = True
        
        self.iterations = config.closOrbIterations
         
        # self.textData = []
         
        self.orbitCanvas = canvas
        self.orbitFigure = figure
         
         
        self.myOrb = closedOrbit(plotErrorFlag=plotErrors)
        self.histoFile = self.myOrb.coOrbitMaxFile
         
        self.correctionText = self.myOrb.correct()
         
         
    def plotOrbit(self):
        def plotTrajectory():
            # print(self.myOrb.orbitUncorData)
            sUncor = self.myOrb.orbitUncorData[0]
            xUncor = self.myOrb.orbitUncorData[1]
             
            sCor = self.myOrb.orbitCorData[0]
            xCor = self.myOrb.orbitCorData[1]
             
            self.orbitCanvas = FigureCanvas(self.orbitFigure)  # connect "figure" to "canvas"
            ax = self.orbitFigure.add_subplot(111)
     
            lw = 2
     
            ax.plot(sUncor, xUncor, label='Orbit, uncorrected', lw=lw)
            ax.plot(sCor, xCor, label='Orbit, corrected', lw=lw)
            ax.grid(True)
            ax.legend()
            ax.set_xlabel("path length [m]")
            ax.set_ylabel("Deviation x [m]")
             
            self.orbitCanvas.draw()
             
        def plotHistogram():
            self.plotData = np.loadtxt(self.histoFile, skiprows=8)
            xUncor, yUncor, xCor, yCor = self.plotData.T[0], self.plotData.T[1], self.plotData.T[2], self.plotData.T[3]
            
            dataToPrint = '\nLattice: pb73\nAll errors\n'
            dataToPrint += 'Average of:\n xUncor = %f \n yUncor = %f \n xCor = %f \n yCor = %f' % \
                   (np.average(xUncor), np.average(yUncor), np.average(xCor), np.average(yCor))
            print(dataToPrint)
            with open('reportData.txt', 'a') as f:
                f.write(dataToPrint)
             
            self.orbitCanvas = FigureCanvas(self.orbitFigure)  # connect "figure" to "canvas"
            ax1 = self.orbitFigure.add_subplot(211)
             
            histoBins = 20
            histoColors = ['r', 'g']
             
            # print(self.textData)
            # print(xUncor)
            # print(xCor)
            ax1.hist([xUncor, xCor], color=histoColors, bins=histoBins, label=['Uncorrected horizontal', 'Corrected horizontal'])
            # Make the ticks integer
            ax1.yaxis.set_major_locator(MaxNLocator(integer=True))
             
            # TODO: Think on how to add the y orbit as well
            ax1.legend()
             
            ax1.set_xlabel("Maximum closed orbit deviation X, [m]")
            ax1.set_ylabel("Number of cases")
             
            ax2 = self.orbitFigure.add_subplot(212)
            ax2.hist([yUncor, yCor], color=histoColors, bins=histoBins, label=['Uncorrected vertical', 'Corrected vertical'])
             
            # Make the ticks integer
            ax2.yaxis.set_major_locator(MaxNLocator(integer=True))
             
            ax2.legend()
             
            ax2.set_xlabel("Maximum closed orbit deviation Y, [m]")
            ax2.set_ylabel("Number of cases")
             
            self.orbitCanvas.draw()
             
                         
 
        if self.iterations == 1:
            plotTrajectory()
             
        elif self.iterations > 1:
            plotHistogram()
             
        return self.orbitCanvas
     
if __name__ == "__main__":
    import timeit
    co = closedOrbit()
    print('time = %f seconds' % timeit.timeit(co.correct, number=1))  # Do not change the number
    # timeit.Timer(co.correct).timeit()
    # co.correct()
    #===========================================================================
    # with open(co.coOrbitMaxFile, 'r') as f:
    #     print(f.read())
    #===========================================================================
