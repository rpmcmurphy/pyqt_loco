'''
Created on 11 Nov 2015

@author: localadmin_okovalen
'''

import numpy as np
import math

#from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
#from matplotlib.figure import Figure
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator # for making the integer ticks

class KickersStrengthPlot(object):
    """ This class object is executed after the closed orbit correction.
    It will plot the strengths of the kickers needed for the correction """
    def __init__(self):
        self.loc = []
        self.kickStrengthHor = []
        self.kickStrengthVer = []
        
    def findBetweenAB(self, s, a, b):
        start = s.index(a) + len(a)
        end = s.index(b)
        return s[start:end].strip()
    
    def readFile(self, fileName = 'stren.out'):
        with open(fileName, 'r') as f:
            data = f.readlines()
            #print('\n'.join(data))
            for line in data:
                kickNameEndIdx = line.index(':')
                kickName = line[0:kickNameEndIdx]
                kickVal = float(self.findBetweenAB(line, '=', ';'))
                
                #print(kickName[0:4])
                if 'kixy' in kickName[0:4]:
                    self.kickStrengthHor.append(kickVal)
                    self.kickStrengthVer.append(kickVal)
                elif 'kix' in kickName[0:3]:
                    self.kickStrengthHor.append(kickVal)
                elif 'kiy' in kickName[0:3]:
                    self.kickStrengthVer.append(kickVal)
            #print(self.kickStrengthHor, self.kickStrengthVer)
                
    #===========================================================================
    # def plotHisto(self):
    #     coef = 1000 # multiply to get mrad
    #     x = np.multiply(self.kickStrengthsHor, coef)
    #     print(x)
    #     plt.hist(x, bins = 20)
    #     plt.xlabel('Strength [mrad]')
    #     plt.ylabel('N of kickers')
    #     #plt.legend()
    #     plt.show()
    #===========================================================================
        
class KickersStrengthPlotWindow(object):
    def __init__(self, canvas, figure):
        self.orbitCanvas = canvas
        self.orbitFigure = figure
        
        self.kickerPlot = KickersStrengthPlot()
        self.kickerPlot.readFile()
        
        
    def plotHisto(self):
        #ax = self.orbitFigure.add_subplot(111)
        ax1 = self.orbitFigure.add_subplot(211)
        
        coef = 1000 # multiply to get mrad
        strHor = np.multiply(self.kickerPlot.kickStrengthHor, coef)
        
        #print(x)
        ax1.hist(strHor, bins = 20, label = 'Horizontal', color = 'b')
        ax1.set_xlabel('Strength [mrad]')
        ax1.set_ylabel('N of kickers')
        # Make the ticks integer
        ax1.yaxis.set_major_locator(MaxNLocator(integer = True))
        # print(strHor)
        #ax1.set_xticklabels(range(np.min(strHor), np.ceil(max(strHor))+1))
        ax1.legend()
        
        
        strVer = np.multiply(self.kickerPlot.kickStrengthVer, coef)
        
        ax2 = self.orbitFigure.add_subplot(212)
        
        coef = 1000 # multiply to get mrad
        x = np.multiply(self.kickerPlot.kickStrengthVer, coef)
        #print(x)
        ax2.hist(x, bins = 20, label = 'Vertical', color = 'r')
        ax2.set_xlabel('Strength [mrad]')
        ax2.set_ylabel('N of kickers')
        # Make the ticks integer
        ax2.yaxis.set_major_locator(MaxNLocator(integer = True))
        
        #ax2.set_xticklabels(range(np.min(strVer), np.ceil(max(strVer))+1))
        ax2.legend()
        
        #plt.legend()
        self.orbitCanvas.draw()
        return self.orbitCanvas
        

            
if __name__ == '__main__':
    kick = KickersStrengthPlot()
    kick.readFile()
    
    def plotHisto():
        coef = 1000 # multiply to get mrad
        x = np.multiply(kick.kickStrengthsHor, coef)
        #print(x)
        plt.hist(x, bins = 20)
        plt.xlabel('Strength [mrad]')
        plt.ylabel('N of kickers')
        #plt.legend()
        plt.show()
        
    plotHisto()
