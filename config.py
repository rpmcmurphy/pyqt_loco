""" FileFirst goes directly into mad-x.exe.
FileSecond is called by the FileFirst.
"""

from configobj import ConfigObj
configFile = 'config.ini'
Config = ConfigObj(configFile, file_error = True)
Config.filename = configFile

first = Config['Lattice']['firstFile']
second = Config['Lattice']['secondFile']

madxFile = Config['Files']['madx']
launchBatFile = Config['Files']['launch']
twissFile = Config['Files']['twiss']
dumpFile = Config['Files']['dump']

plotMagnetsFlag = Config['Plot flags']['plotMagnets']
closOrbIterations = int(Config['Closed orbit correction']['closOrbIterations'])
# Config is a dictionary. 'get' is a python function for dictionaries. 'as_bool' is a configobj term
plotMagnetMisalignRoll = Config.get('Closed orbit correction').as_bool('plotMagnetMisalignRoll')

ORMIterations = int(Config['ORM analysis']['ORMIterations'])

calcMethod = Config['ORM analysis']['calcMethod']
quadStrengths = Config['ORM analysis']['quadStrengths']
quadErrorNames = Config['ORM analysis']['quadErrorNames']
quadErrorValues = Config['ORM analysis']['quadErrorValues']
plotMatrRdiff = Config.get('ORM analysis').as_bool('plotMatrRdiff')
kickStrengths = Config['ORM analysis']['kickStrengths']
kickMagnets = Config['ORM analysis']['kickMagnets']
expMatrRCalcMethod = Config['ORM analysis']['expMatrRCalcMethod']
kickFile = Config['ORM analysis']['kickFile']
couplingOn = Config.get('ORM analysis').as_bool('couplingOn')
errorBPM = Config['ORM analysis']['errorBPM']


showStartup = Config['Startup window']['show']
 
 
#===============================================================================
# import configparser
# 
# configFile = 'config.ini'
# 
# Config = configparser.ConfigParser()
# Config.read(configFile)
# 
# first = Config.get('Lattice', 'firstFile')
# second = Config.get('Lattice', 'secondFile')
# 
# madxFile = Config.get('Files', 'madx')
# launchBatFile = Config.get('Files', 'launch')
# twissFile = Config.get('Files', 'twiss')
# dumpFile = Config.get('Files', 'dump')
# 
# plotMagnetsFlag = Config.get('Plot flags', 'plotMagnets')
# closOrbIterations = Config.getint('Closed orbit correction', 'closOrbIterations')
# 
# ORMIterations = Config.getint('ORM analysis', 'ORMIterations')
# quadStrengths = Config.get('ORM analysis', 'quadStrengths')
# quadErrorNames = Config.get('ORM analysis', 'quadErrorNames')
# quadErrorValues = Config.get('ORM analysis', 'quadErrorValues')
# 
# showStartup = Config.get('Startup window', 'show')
#===============================================================================

# RawConfig = configparser.RawConfigParser()

#===============================================================================
# first = 'CR_lattice.txt'
# second = 'CR_work.txt'
# # Uncomment the next two lines for the HESR and comment the previous 2 lines
# #first = 'Hesr_CR_injection_StSx.txt'
# #second = 'HesrAperture_CR_core_StSx.txt'
# 
# twissFile = 'twiss.txt'
# dumpFile = 'dump.txt'
# launchBatFile = 'Launch.bat'
# madxFile = 'madx.exe'
#===============================================================================




if __name__ == '__main__':
    #print(first)
    #print(plotMagnetsFlag)
    #print(closOrbIterations)
    #print(type(quadErrorNames[0]), quadStrengths, quadErrorValues)
    #print([val.strip() for val in quadStrengths.split(',')])
#===============================================================================
#     print(first)
#     print(quadStrengths)
#     print(twissFile)
#     print(closOrbIterations, int(closOrbIterations)+ 10)
#     print(plotMagnetMisalignRoll)
#     print(type(plotMagnetMisalignRoll))
#     print(Config.get('Closed orbit correction'))
# 
#     #print(kickStrengths.replace('\t', ''))
#     #kickStrengths.replace(' ', '')
#     #kickStrengths.replace("\t", 'asdf')
#     print(kickStrengths)
#     print(type(calcMethod))
#     print(type(expMatrRCalcMethod))
#     print(couplingOn)
#===============================================================================
#     print(errorBPM, type(errorBPM))
    #===========================================================================
    # if errorBPM == 'False':
    #     print('False')
    # else:
    #     print('it is not false:', errorBPM)
    #===========================================================================
#     print(type(errorBPM), errorBPM.isdigit())
    try:
        if errorBPM != 'False' and float(errorBPM):
            print(errorBPM)
    except:
        raise Exception('There is a problem with errorBPM value in config.ini!')
    
    import numpy as np
    print(np.random.uniform(-float(errorBPM), float(errorBPM)))

    
    #===========================================================================
    # print(kickMagnets, len(kickMagnets))
    # print(len(kickStrengths))
    # for i, val in enumerate(kickStrengths):
    #     print(kickMagnets[i], kickStrengths[i])
    #===========================================================================
    
