'''
Every each class except FilesExistTest launches the <Launch.bat> which in its turn
launches the mad.exe and produces the <twiss.txt> and <dump.txt>. On the destruction 
of each class (tearDownClass) the <dump.txt> file is removed
30.09.15
'''

#DONE: Consider PyUnit instead of unittest???. PyUnit and unittest are the same...

import unittest
import os

from main import * # class mainWindow also
from ReadTwiss import readTwiss
from ParseLatticeFile import parseLattice
from Calculations import calculations
import config
#from MadxExecution import madxExecution

stars = 25 # formatting style

cwd = os.getcwd()
#print(cwd)
#print(os.path.join(cwd, twissFile))

def removeDump():
    if os.path.isfile(config.dumpFile):
        os.remove(config.dumpFile)
        
def setupProgram():
    app = QApplication(sys.argv)
    w = mainWindow()
    w.madxExec.launchBat(config.launchBatFile)
    return w
        
# tearDownModule will be executed once after finishing all tests
def tearDownModule():
    # remove 'dump.txt', which is created after 'Launch.bat'
    removeDump()
    pass
    #print("Ending testing environment for 'main' module")

class MainModuleTests(unittest.TestCase):
    @classmethod
    def tearDownClass(cls):
        removeDump()
        print("\nExit MainModuleTest >>> \n\n", stars*"*", "\n")
    # First define a class variable that determines 
    # if setUp was ever run
    
    ClassIsSetup = False
    def setUp(self):
        # If it was not setup yet, do it
        if not self.ClassIsSetup:
            print("<<< Start MainModuleTest")
            # run the real setup
            self.setupClass()
            # remember that it was setup already
            self.__class__.ClassIsSetup = True
                               
    def setupClass(self):
        unittest.TestCase.setUp(self)
        self.__class__.w = setupProgram()
    
    # My tests for 'main' module
    def test_dump_txt_after_Launch(self):
        self.assertTrue(os.path.exists(os.path.join(cwd, config.dumpFile)) == 1,
                         "dump.txt missing!")

    def test_twiss_txt_after_Launch(self):
        #print(os.path.exists(os.path.join(cwd, config.twissFile)))
        self.assertTrue(os.path.exists(os.path.join(cwd, config.twissFile)) == 1,
                 "twiss.txt missing!")
    
    def test_lattice_quads_not_zero(self):
        latticeNum = self.w.showInfo()
        self.assertTrue(latticeNum[0]['quad'] > 0)


class ReadTwissTests(unittest.TestCase):
    @classmethod
    def tearDownClass(cls):
        removeDump()
        print("\nExit ReadTwissTest >>> \n\n", stars*"*", "\n")
    # First define a class variable that determines 
    # if setUp was ever run
    
    ClassIsSetup = False
    def setUp(self):
        # If it was not setup yet, do it
        if not self.ClassIsSetup:
            print("<<< Start ReadTwissTest")
            # run the real setup
            self.setupClass()
            # remember that it was setup already
            self.__class__.ClassIsSetup = True
                               
    def setupClass(self):
        unittest.TestCase.setUp(self)
        setupProgram()
        self.__class__.twiss = readTwiss(config.twissFile)
        
    def test_data_after_read_not_none(self):
        rawData = self.twiss.readFile(config.twissFile)
        self.assertTrue(rawData is not None)
        
class CalculationTests(unittest.TestCase):
    @classmethod
    def tearDownClass(cls):
        removeDump()
        print("\nExit CalculationsTest >>> \n\n", stars*"*", "\n")
    # First define a class variable that determines 
    # if setUp was ever run
    
    ClassIsSetup = False
    def setUp(self):
        # If it was not setup yet, do it
        if not self.ClassIsSetup:
            print("<<< Start CalculationsTest")
            # run the real setup
            self.setupClass()
            # remember that it was setup already
            self.__class__.ClassIsSetup = True
                               
    def setupClass(self):
        unittest.TestCase.setUp(self)
        w = setupProgram()
        
        #self.__class__.data, self.__class__.latInfo = \la
        #    w.calc.readTwiss.readTwissFile(config.twissFile)
        self.__class__.latParse = parseLattice()
        self.__class__.calc = calculations()
        self.calc.twissToData(config.twissFile)
        self.calc.findKickAndMonLengths(self.calc.data['KEYWORD'], self.calc.hkickNames, self.calc.vkickNames)
    
    def test_model_r_matrix_not_none(self):
        #matr = self.calc.matrR(self.data, self.latInfo)
        matrModel = self.calc.calcMatrRmodel(config.calcMethod)
        #print(matr)
        self.assertTrue(matrModel is not None)
        
    def test_quad_errors_lattice(self):
        fileReadEnding = ''
        fileEndingExp = '_exp'
        quadStrengthNew = self.latParse.errorLattice(self.calc.QuadErrNames , self.calc.QuadErrVals, 'absolute', '', fileEndingExp)
            
    def test_exp_r_matrix_not_none(self):
        matrExp = self.calc.measureMatrRexp(config.calcMethod)
        self.assertTrue(matrExp is not None)
    
        #print(matrExp)
        
    #TODO: also do the test for the 'nonlinearSlow" calcMethod
    
    
    #===========================================================================
    # def test_j_matrix_not_none(self):
    #     matrModel = self.calc.calcMatrRmodel()
    #     matrExp = self.calc.measureMatrRexp()
    #     matrJ = self.calc.self.calcJmatrNoArgs()
    #     self.assertTrue(matrJ is not None)
    #===========================================================================
        

class FilesExistTests(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        print("<<< Start FilesExistTest")
    
    @classmethod
    def tearDownClass(cls):
        removeDump()
        print("\nExit MainModuleTest >>> \n\n", stars*"*", "\n")
    
    """ Test if the needed files are located in the project folder """
    def test_madx_exe_exists(self):
        self.assertTrue(os.path.exists(os.path.join(cwd, config.madxFile)))
        
    def test_Launch_bat_exists(self):
        self.assertTrue(os.path.exists(os.path.join(cwd, config.launchBatFile)))
        
    def test_first_madx_lattice_file_exists(self):
        self.assertTrue(os.path.exists(os.path.join(cwd, config.first)))
        
        
    

def main():
    unittest.main()
if __name__ == "__main__":
    main()
