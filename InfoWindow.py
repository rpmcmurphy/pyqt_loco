'''
Created on 9 Oct 2015

@author: localadmin_okovalen
'''

from PyQt5.QtWidgets import QMainWindow, QTextEdit, QSizePolicy, QPlainTextEdit
from PyQt5.QtCore import Qt

class infoWindow(QMainWindow):
    def __init__(self, text):
        super().__init__()
        
        self.setWindowTitle("Help Information")
        self.setGeometry(100, 100, 500, 400)
        self.setAttribute(Qt.WA_DeleteOnClose)
        infoText = QTextEdit(text)
        infoText.setReadOnly(True)
        
        self.setCentralWidget(infoText)
        
class infoTab(QPlainTextEdit):
    def __init__(self, text):
        super().__init__()
        self.setAttribute(Qt.WA_DeleteOnClose)
        self.setReadOnly(True)
        
        self.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Expanding)
        
        self.setPlainText(text)
        

        