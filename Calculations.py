import os

import numpy as np
from numpy import sqrt, pi, sin, cos, abs
from PyQt5 import QtCore

from ParseLatticeFile import parseLattice
from ReadTwiss import readTwiss
import config
from MadxExecution import madxExecution
from plotMatrR import histo3d



""" Theory:
Eq.1: bpmDataVec(m x 1) = matrRmodel(m x n) * kickVec(n x 1)
1) matrRmodel is found from mad-x lattice (bx, by, mux, muy, Qx, Qy are needed)
2) matrRexp is found By varying each of the kickers separately and then finding
the corresponding exp-Rmatrix elements:
R_11 = x_1/kick_1, R_m1 = x_m/kick_m, R_1n = x_1/kick_n, R_mn = x_m/kick_n
3) Now we can find matrRdiff = matrRmodel - matrRexp. Then we build
a vector [vecRdiff_11, Rdiff_1m, Rdiff_n1, Rdiff_nm]
Eq.2: vecRdiff (n*m x 1) = matrJgrad (n*m x N) * errVec(N x 1)
- errVec consists of quad grad errors + (magnets rolls etc.)
matrJgrad consists of \partial R / \partial errV or:
matrJgrad[1,1] = \part R_11 / \part errV_11,
matrJgrad[m,1] = \part R_mn / \part errV_11,
matrJgrad[1,n] = \part R_11 / \part errV_N,
matrJgrad[m,n] = \part R_mn / \part errV_N.
matrJgrad can be found by, e.g. computing the change in R_ij for small
variation of the errV_k (e.g. for small quad gradient change)
4) Now we invert the Eq.2: errVec = J^(-1) * vecRdiff.
vecRdiff we know, J^(-1) is found by SVD: this leads to identification 
of the errVec and finding the errors in the lattice.
"""

    

#class calculations():
class calculations(QtCore.QObject):
    """ The class is inherited from the QtCore.QObject in order to be able to emit 
    signals. The signal will be emited during calculations. It is to be intercepted by the
    childWindow which will be refreshed and show the information"""
    updatedCalcSignal = QtCore.pyqtSignal(str) # for updating the result string after calculations
    visualORMMatrSignal = QtCore.pyqtSignal(np.ndarray, int, int) # for visualising the ORM matrix
    #visualORMMatrSignal = QtCore.pyqtSignal(str) # for visualising the ORM matrix
    # Then the information window can be updated.
    
    def __init__(self):

        QtCore.QObject.__init__(self)
        self.madxExec = madxExecution()
        
        self.readTwiss = readTwiss(config.twissFile)
        self.latParse = parseLattice()
        
        
        
        def setupReadTwiss():
            self.data = []
            self.latInfo = []
            
            if not os.path.exists(os.path.join(os.getcwd(), config.twissFile)):
                self.madxExec.launchMadx()
            self.twissToData(config.twissFile)
            

            
            
        def setupORM():
            #TODO: Think on providing the measuring of the coupling matrix elements
            #self.quadStrengthsNames = ['KQD', 'KQF1', 'KQF2', 'KQF3'] # uncomment this for HESR
            #self.QuadErrNames = ['KQF2', 'KQF3'] # uncomment this for HESR 
            # Simulate the errors in the quadrupoles
            
            self.quadStrengthsNames = config.quadStrengths
            self.QuadErrNames = config.quadErrorNames
            self.QuadErrVals = config.quadErrorValues
                        
            
            self.iter = config.ORMIterations

            self.Vdiff = np.zeros(len(self.quadStrengthsNames)) # Quad errors which are found via ORM
            self.quadEpsilon = '0.00001' # starting precision for J matrix calculation (quad error)
            self.sumOfSquares = [] # Sum of squares of Rdiff
            
            self.kickEpsilon = 0.001 # for varying the kickers for an ORM matrix calculation in matrSlow
            
            #kickers
            self.hkickNames, self.hkickStrengths, self.vkickNames, self.vkickStrengths = \
                self.latParse.findKickNamesAndStrengths()
            
            """ Total number of horizontal - 29, vertical - 24
            Plotting the diffMatr is possible only for max = 25 hor and 0 ver, for example.
            Otherwise there is a MemoryError."""
            #TODO: Put this (number of hor and ver correctors to be used) into the config.ini
            numOfHor = 29
            numOfVer = 0
            #numOfHor = 25
            self.hkickNames, self.hkickStrengths = self.hkickNames[:numOfHor], self.hkickStrengths[:numOfHor]
            #numOfVer = 12
            self.vkickNames, self.vkickStrengths = self.vkickNames[:numOfVer], self.vkickStrengths[:numOfVer]
            
            #self.vkickNames, self.vkickStrengths = [], []
            
            # monitors
            self.xMonNames = [b'"HMONITOR"', b'"MONITOR"']
            self.yMonNames = [b'"VMONITOR"', b'"MONITOR"']
            
            self.ORMResultDump = 'The quadrupoles under investigation are:\n' + ', '.join(self.quadStrengthsNames) +\
            '.\nThe strengths of the following quadrupoles:\n' + ', '.join(self.QuadErrNames) +\
            '\nwere changed to \n' + ', '.join(self.QuadErrVals) + \
            '.\nLet us check whether we will find the correct strengths.\n' +\
            '\n\n\n\n'
            
            #self.sumOfSquaresDif = 99999999 # this is the difference between old and new
            #self.plotMatrRdiffHisto = histo3d()
            self.visualORMMatr = histo3d()

        setupReadTwiss()
        # setupPlotTwiss()
        setupORM()
        
    
    def twissToData(self, fname):
        self.data, self.latInfo = self.readTwiss.readTwissFile(fname)
        #Get number of magnetic elements in lattice - file "twiss.txt"
        #self.showInfo()
        self.readTwiss.numberOfMagnElements()
    

    def findKickAndMonLengths(self, magnType, xKickNames, yKickNames):
        """ - Kickers: Kickers names are passed as parameters to find their length.
              Lattice file is used to find the kickers names
            - Monitors: magnType (list of magnet types from the twiss file) is used
            to find the number of monitors.
              Twiss file is used for finding the monitors
        We only pass the magnType as a first parameter. It will be used to
        find the length of the monitors.
        names of the kickers as the parameters"""   
             

        
        self.xKickLen = len(xKickNames)
        self.yKickLen = len(yKickNames)
        self.kickLen = self.xKickLen + self.yKickLen
        
        
        self.xMonLen = len([x for x in magnType if x in self.xMonNames])
        self.yMonLen = len([x for x in magnType if x in self.yMonNames])
        # If we don't include the hor kickers remove the hor monitors as well
        if self.xKickLen == 0:
            self.xMonLen = 0
        # If we don't include the ver kickers remove the ver monitors as well            
        if self.yKickLen == 0:
            self.yMonLen = 0
        self.monLen = self.xMonLen + self.yMonLen
        
        
        print('xmonlen, yMonlen, monLen, kickLen', self.xMonLen, self.yMonLen, self.monLen, self.kickLen)
        
#         return self.xMonLen, self.yMonLen, self.xKickLen, self.yKickLen
        

    def matrRfast(self, data, latInfo):
        """ Returns: ORM matrix calculated from read-out-from-twiss bx, mux, by, muy 
            Arguments: twiss data """
        def assignBetaAndMuAndDispAtMon(magnType, bx, mux, dx, by, muy, dy):
            bxAtMon, muxAtMon, dxAtMon, byAtMon, muyAtMon, dyAtMon = [], [], [], [], [], []
            for i, element in enumerate(magnType):
                    #if element == b'"HMONITOR"' or element == b'"MONITOR"':
                    if element == b'"HMONITOR"' and self.xMonLen != 0: # xMonLen != 0 is only to exclude the hor monitors
                    #if element == b'"HMONITOR"':
                        bxAtMon.append(bx[i])
                        muxAtMon.append(mux[i])
                        dxAtMon.append(dx[i])
                        #print('monitor', bx[i], mux[i])
                    elif element == b'"VMONITOR"' and self.yMonLen != 0:
                        byAtMon.append(by[i])
                        muyAtMon.append(muy[i])
                        dyAtMon.append(dy[i])
                    elif element == b'"MONITOR"':
                        if self.xMonLen != 0: # only to exclude the horizontal kickers and monitors from calculations
                            bxAtMon.append(bx[i])
                            muxAtMon.append(mux[i])
                            dxAtMon.append(dx[i])   
                        if self.yMonLen != 0:
                            byAtMon.append(by[i])
                            muyAtMon.append(muy[i])
                            dyAtMon.append(dy[i])
            return bxAtMon, muxAtMon, dxAtMon, byAtMon, muyAtMon, dyAtMon

        def assignBetaAndMuAndDispAtKick(kickType, kickMagnNames, beta, mu, disp):
            bAtKick, muAtKick, dispAtKick = [], [], []
            for kickMagn in kickMagnNames:
                for i, name in enumerate(names):
                    if name[1:-1].lower() == str.encode(kickMagn): # we cut "" from the name
                        if magnType[i] == kickType or magnType[i] == b'"KICKER"':
                            #print(hkickMagn, magnType[i], len(bxAtHkick), 'x')
                            bAtKick.append(beta[i])
                            muAtKick.append(mu[i])
                            dispAtKick.append(disp[i])
                            
            return bAtKick, muAtKick, dispAtKick
        
        def assignMatr(bxAtMon, muxAtMon, dxAtMon, byAtMon, muyAtMon, dyAtMon, \
                       bxAtHkick, muxAtHkick, dxAtHkick, byAtVkick, muyAtVkick, dyAtVkick, \
                       Qx, Qy, alfaCompaction, lorentz, circum):
            MatrRmodel = np.full([self.monLen, self.kickLen], np.nan)
            #TODO: so far it is the formula for "x" plane without dispersion term
            #monLen = self.xMonLen + self.yMonLen
            #kickLen = self.xKickLen, self.yKickLen
            for i in range(self.monLen):
                for j in range(self.kickLen):
                    if i < len(bxAtMon) and j < len(bxAtHkick):
                        dispTermX = dxAtMon[i]*dxAtHkick[j]/((alfaCompaction - 1/lorentz**2)*circum)
                        dispTermX = 0
                        #print('i, j, dispTermX', i, j, dispTermX)
                        MatrRmodel[i][j] = sqrt(bxAtMon[i] * bxAtHkick[j]) * \
                            cos(abs(2 * pi * (muxAtMon[i] - muxAtHkick[j])) - pi * Qx) \
                            / (2 * sin(pi * Qx)) - dispTermX
                    elif i >= len(bxAtMon) and j < len(bxAtHkick):
                        MatrRmodel[i][j] = 0
                    elif i < len(bxAtMon) and j >= len(bxAtHkick):
                        MatrRmodel[i][j] = 0
                    elif i >= len(bxAtMon) and j >= len(bxAtHkick):
                        dispTermY = dyAtMon[i - self.xMonLen]*dyAtVkick[j - self.xKickLen]/((alfaCompaction - 1/lorentz**2)*circum)
                        dispTermY = 0
                        MatrRmodel[i][j] = sqrt(byAtMon[i - self.xMonLen] * byAtVkick[j - self.xKickLen]) * \
                            cos(abs(2 * pi * (muyAtMon[i - self.xMonLen] - muyAtVkick[j - self.xKickLen])) - pi * Qy) \
                            / (2 * sin(pi * Qy)) - dispTermY
                        
            return MatrRmodel
            
            
            
        names, magnType, bx, mux, by, muy, dx, dy = \
            data['NAME'], data['KEYWORD'], data['BETX'], data['MUX'], data['BETY'], data['MUY'], data['DX'], data['DY']
        Qx, Qy  = latInfo['Q1'], latInfo['Q2']
        alfaCompaction = latInfo['ALFA']
        #lorentz = latInfo['ENERGY']/latInfo['MASS']
        lorentz = latInfo['GAMMA']
        circum = latInfo['LENGTH']
        #print(dx)
        
        
        #print('xkicklen, xmonlen', xKickLen, xMonLen)
        #print('monLen, kickLen: ', monLen, kickLen)
        
        bxAtMon, muxAtMon, dxAtMon, byAtMon, muyAtMon, dyAtMon = assignBetaAndMuAndDispAtMon(magnType, bx, mux, dx, by, muy, dy)
        #print('dxAtmon, dyatmon', dxAtMon, dyAtMon)
        
        """ The sequential order matters! It should be the same as the matrRexp, horizontal kickers
        first, then vertical kickers. Therefore we have 2 cycles."""
        bxAtHkick, muxAtHkick, dxAtHkick = assignBetaAndMuAndDispAtKick(b'"HKICKER"', self.hkickNames, bx, mux, dx)
        byAtVkick, muyAtVkick, dyAtVkick = assignBetaAndMuAndDispAtKick(b'"VKICKER"', self.vkickNames, by, muy, dy)
        
        #print('dispXAtHkick, dispYAtVkick', dxAtHkick, dyAtVkick)
        
        # convert to np.array
        bxAtMon, muxAtMon, byAtMon, muyAtMon = \
            np.array(bxAtMon), np.array(muxAtMon), np.array(byAtMon), np.array(muyAtMon)
        bxAtHkick, muxAtHkick, byAtVkick, muyAtVkick = \
            np.array(bxAtHkick), np.array(muxAtHkick), np.array(byAtVkick), np.array(muyAtVkick)
        
        
        
        MatrRmodel = assignMatr(bxAtMon, muxAtMon, dxAtMon, byAtMon, muyAtMon, dyAtMon, \
                                bxAtHkick, muxAtHkick, dxAtHkick, byAtVkick, muyAtVkick, dyAtVkick, \
                                Qx, Qy, alfaCompaction, lorentz, circum)
        
        #print(MatrRmodel)
        np.savetxt('matrRexpTestFast.txt', MatrRmodel)
        return MatrRmodel
    
    def matrRslow(self, inputLatticeFileEnding):
        """ If inputLatticeFileEnding == '' then the model lattice CR_lattice is used. This is the
        case only for calculating the model matr R."""

        matr = np.full([self.monLen, self.kickLen], np.nan)
        
        """ Start changing the kickers to find the R-matrix"""
        kickStrengths = self.hkickStrengths + self.vkickStrengths
        for i, kickErrorName in enumerate(kickStrengths):
            #TODO: change _mod to outputFileLatticeEnding or smth...
            fileEndingKick = '_' + kickErrorName
            self.latParse.errorLattice(kickErrorName, self.kickEpsilon, 'absolute', inputLatticeFileEnding, fileEndingKick)
            self.madxExec.launchBat('Launch' + fileEndingKick + '.bat')
            data, latInfo = self.readTwiss.readTwissFile('twiss' + fileEndingKick + '.txt')
            
            xAtXMon, yAtYMon = [], []
            if self.xMonLen != 0:
                xAtXMon = [x for x,keyword in zip(data['X'], data['KEYWORD']) if keyword in self.xMonNames]
            if self.yMonLen != 0:
                yAtYMon = [y for y,keyword in zip(data['Y'], data['KEYWORD']) if keyword in self.yMonNames]
            
            
            try:
                if config.errorBPM != 'False':
                    errorBPM = float(config.errorBPM)
                    np.random.seed(0)
                    #print('xAtXMOn before:', xAtXMon)
                    xAtXMon = [val + np.random.uniform(-errorBPM, errorBPM) for val in xAtXMon]
                    #print('xAtXMOn after:', xAtXMon)
                    yAtYMon = [val + np.random.uniform(-errorBPM, errorBPM) for val in yAtYMon]
                else:
                    pass
            except:
                raise Exception('There is a problem with errorBPM value in config.ini!')
            
                
            
            # To find the matrix elements we divide the orbit onto the kick strength
            matrCol = np.divide((xAtXMon + yAtYMon), self.kickEpsilon)
            
            #print('xMax = ', max(self.dataErr['X']))
            #print('yMax = ', max(self.dataErr['Y']))
            
            matr[:,i] = matrCol
        #print('matr', matr, matr.shape)
        
        """ if we have the matrix M x N, where M - number of rows, N - number of columns
            for the full matrix M = xMonLen + yMonLen, N = xKickLen + yKickLen
            so the upper-left quadrant is xMonLen x xKickLen; matr[0,xMonLen:
            upper-right quadrant is xMonLen x yKickLen
            bottom-left quadrant is yMonLen x yKickLen
            bottom-right quadrant is yMonLen x yKickLen
        """
        if not config.couplingOn: # if we neglect coupling 
            if self.xMonLen != 0 and self.yMonLen != 0: # full matrix
                matr[0:self.xMonLen, self.xKickLen:] = 0 # upper right quadrant
                matr[self.xMonLen:, 0:self.xKickLen] = 0 # bottom left quadrant
                
            
        
        np.savetxt('matrRexpRealSlow.txt', matr)
        
        return matr


#TODO: Too many matrRdiff (3 functions - get rid of some).



    
    #TODO: Maybe this is a redundant function. I should check where I use it and how often. 
    #TODO: Model matrix is calculated only once and from the twiss parameters, no??
    def calcMatrRmodel(self, calcMethod):
        #self.matrRmodel = self.matrRfast(self.data, self.latInfo)
        if config.calcMethod == 'linearFast':
            self.matrRmodel = self.matrRfast(self.data, self.latInfo)
        elif config.calcMethod == 'nonlinearSlow':
            self.matrRmodel = self.matrRslow('')
        print('model', self.matrRmodel)
        return self.matrRmodel


        #self.statusBarMessage('Calculate matrix MatrRmodel for Orbit Response Analysis')
    
    def measureMatrRexp(self, calcMethod): #calcMethod can be "linearFast" or "nonlinearSlow"
        """ Returns: ORM matrix of the lattice with introduced quadrupoles errors.
        Method1: Linear, fast. The lattice with quad errors is loaded into the MAD-X and the 
        beta-functions together with phases are read to calculate the matrix elements.
        Method2: Nonlinear, slow. We vary each corrector and read out the data from the BPMs. After
        varying all the correctors we find the ORM matrix elements X/Theta
        """
        #TODO: So far the exp matrix is calculated as though we know the twiss file! But it is not
        #TODO: true in reality. It should be calculated from closed-orbit-response-from-kicks
        
        #print(message1, message2)
        
        fileEndingExp = '_exp'
        self.latParse.errorLattice(self.QuadErrNames , self.QuadErrVals, 'absolute', '', fileEndingExp)
        
        if calcMethod == 'linearFast':
            if config.expMatrRCalcMethod == 'test':
                # Launch madx, create twiss file
                self.madxExec.launchBat('Launch' + fileEndingExp + '.bat')
                data, latInfo = self.readTwiss.readTwissFile('twiss' + fileEndingExp + '.txt')
                # For now I will just take the values from the Twiss file
                # In reality the R-coeffs come from the kicks and corresp. BPM values
                self.matrRexp = self.matrRfast(data, latInfo)
            elif config.expMatrRCalcMethod == 'real':
                self.matrRexp = self.matrRslow(fileEndingExp)
            
        elif calcMethod == 'nonlinearSlow':
            self.matrRexp = self.matrRslow(fileEndingExp)
            
        print('experiment', self.matrRexp)        
        return self.matrRexp
       
    def calcMatrRdiff(self, matr1, matr2):
        self.matrRdiff = np.subtract(matr1, matr2)
        print('matrRdiff', self.matrRdiff)
        #self.matrRdiff = self.matrRdiff(matr1, matr2)
        
        return self.matrRdiff 
    
    def calcMatrRdiffNoArgs(self):
        self.matrRdiff = self.calcMatrRdiff(self.matrRmodel, self.matrRexp)
        #self.statusBarMessage('Find difference between matrices R-model and R-exp')
        return self.matrRdiff
        
    def calcJmatr(self, matrR):
        """ 1) Perturb one quadStrength for an epsilon = 0.001 in madxLattice
            "Hesr_CR_injection_StSx.txt" file
            2) Create files_tmp of:
                a) Launch.bat. Also in this file:
                    - change the line
                b) Hesr_CR_injection_StSx.txt. Also in this file:
                    - change "call, file=HesrAperture_CR_core_StSx.txt;"
                c) HesrAperture_CR_core_StSx.txt. Also in this file:
                    - change "twiss.txt"
            3) Launch one tmp, which corresponds to one perturbation, at a time
                a) Calc (Rmodel[ij] - Rexp[ij])/(epsilon*stren) = deltR/deltStren
                 - vector (not matrix) for each of the perturbation.
                Finally, I will obtain deltR[ij] x N, where N is the number of values
                to perturbate (e.g., 12 quadrupoles for beginning)
                b) Build J[ij x N]
        """
        # without KQT0, KQT3B and KQT4B
        # the quantities which will be perturbed
        # Note that KQT and KQTB will be changed simultenaously
        # need to change this bad behaviour
#         quadStrengthsNames = ['KQD', 'KQF1', 'KQF2', 'KQF3', \
#                               'KQC1', 'KQC2', 'KQC3', 'KQC4',\
#                               'KQT1', 'KQT2', 'KQT3', 'KQT4']
        
        # quadStrengthsNames = ['KQC1', 'KQC2', 'KQC3', 'KQC4']
        
        # Find dimensions of matrix J
        def matrJDims():
            """ find dimensions and initialize MatrJ """
            matrRdimRows, matrRdimCols = matrR.shape[0], matrR.shape[1]
            matrJdimRows = matrRdimRows * matrRdimCols
            matrJdimCols = len(self.quadStrengthsNames)
            return matrJdimRows, matrJdimCols
        
        def setFileEndings():
            configFirstFileName, configFirstFileExt = config.first.split('.')  # @UndefinedVariable
            fileEndingTmp = '_tmp'
            endingNew = '_new'
            fileNameNew = configFirstFileName + endingNew + '.' + configFirstFileExt
            """ Check whether the "_new" file exists. During the first iteration
            we don't have a "_new" file yet. So we take the "model" lattice """
            if not os.path.isfile(fileNameNew):
                # First iteration
                fileEndingNew = ''
            else:
                # All other iterations > 0
                fileEndingNew = endingNew
            return fileEndingNew, fileEndingTmp
                
        def findMatrJCol(fileEndingTmp):
            """ If it is the first iteration, we take the values from the model lattice and perturb
            it by creating _tmp files for each perturbation. If it is iteration > 0, then
            we take the values from the "_new" lattice and perturb it by creating "_tmp". 
            Perturbation means changing one of the quad-errors on a small value epsilon and
            finding the R-matrix of this perturbed lattice. Then we find the difference
            between the model matrix R and this perturbed matrix R. Dividing this deltaR
            by an quad-error epsilon we find the elements of one column of the J matrix.
            See http://www.rhichome.bnl.gov/AP/ap_notes/ap_note_267.pdf
            """
            quadStrenNew = self.latParse.errorLattice(quadStrenVal, self.quadEpsilon, 'relative', fileEndingNew, fileEndingTmp)
            self.madxExec.launchBat('Launch' + fileEndingTmp + '.bat')
            dataPerturb, latInfoPerturb = self.readTwiss.readTwissFile('twiss' + fileEndingTmp + '.txt')
            
            if config.calcMethod == 'linearFast':
                matrRPerturb = self.matrRfast(dataPerturb, latInfoPerturb)
            elif config.calcMethod == 'nonlinearSlow':
                matrRPerturb = self.matrRslow(fileEndingTmp)
            
            
            RmatrDel = np.subtract(matrR, matrRPerturb)
            divisor = quadStrenNew * float(self.quadEpsilon)
            #divisor = 1
            RmatrGrad = np.divide(RmatrDel, divisor)
            RvecGrad = RmatrGrad.reshape(matrJDims()[0])
            self.matrJ[:,i] = RvecGrad


        fileEndingNew, fileEndingTmp = setFileEndings()
        self.matrJ = np.full(matrJDims(), np.nan)
        for i, quadStrenVal in enumerate(self.quadStrengthsNames):
            findMatrJCol(fileEndingTmp)

        return self.matrJ

    def calcJmatrNoArgs(self):
        #TODO: Remove all the NoArgs funcs. Instead use lambda expressions to pass the parameter
        matrJ = self.calcJmatr(self.matrRmodel)
        return matrJ
        #self.statusBarMessage('Calculate matrix J by slightly varying the strengths of the' + \
        #                      ' quadrupoles and finding \part R / \part errV')

    def calcMatrRnew(self, calcMethod, fileEnding):
        #TODO: Do I actually need this function?
        """ This function is used for calculating the matrR during the iterations > 0. 
        The lattice is already changed with the new quad strengths K = K + Vdiff found 
        in the previous iteration. """
        
        # Calculate the new twiss based on the new strengths K = K + Vdiff
        self.madxExec.launchBat('Launch' + fileEnding + '.bat')
        dataNew, latInfoNew = self.readTwiss.readTwissFile('twiss' + fileEnding + '.txt')
        # Load new twiss to the matrRfast to calculate the ORM matrix
        if calcMethod == 'linearFast':
            matrRnew = self.matrRfast(dataNew, latInfoNew)
        elif calcMethod == 'nonlinearSlow':
            matrRnew = self.matrRslow(fileEnding)
            
        print('new model', matrRnew)
        
        return matrRnew        
        
    def findQuadErrors(self):
        print('matrJ = ', self.matrJ)
        #======================= SVD ================================================
        # U, s, V = np.linalg.svd(self.matrJ, full_matrices=False)
        # print('U = ', U, U.shape)
        # print('V = ', V, V.shape)
        # print('s = ', s, s.shape)
        # S = np.diag(s)
        # print('S = ', S, S.shape)
        # print('if j = u*s*v.H:', np.allclose(self.matrJ, np.dot(U, np.dot(S, V))))
        # 
        # print('pseudoinverse of S = ', np.linalg.pinv(S))
        #=======================================================================
        #self.matrJinv = np.dot(V.H, np.dot()) 
        #print(self.matrJ)
        self.matrJinv = np.linalg.pinv(self.matrJ)
        #print('self.matrJinv')
        #print('inverse J: ', self.matrJinv, self.matrJinv.shape)
        #print('shape of Rdiff: ', self.matrRdiff.shape)
        #print(self.matrRdiff)
        matrRdiffdimM, matrRdiffdimN = self.matrRdiff.shape[0], self.matrRdiff.shape[1]
        vecRdiff = self.matrRdiff.reshape(matrRdiffdimM * matrRdiffdimN)
        #print('vecRdiff = ', vecRdiff, vecRdiff.shape)
        #self.Vdiff = np.add(np.dot(self.matrJinv, vecRdiff), self.Vdiff)
        #self.Vdiff = np.dot(self.matrJinv, vecRdiff)
        # print(('\n deltaVdiff = ', np.dot(self.matrJinv, vecRdiff)))
        self.Vdiff = np.add(np.dot(self.matrJinv, vecRdiff), self.Vdiff)
        print('Vdiff = ', self.Vdiff)
        
        resultText = ''
        for quad, strength in zip(self.quadStrengthsNames, self.Vdiff):
            resultText += quad + ': ' + str(round(strength, 7)) + '\n'
        resultText += '\n'
        
        print('\nQuadrupole names: ', self.quadStrengthsNames)
        print('Quadrupole errors', self.Vdiff, end = '\n')
        #print(resultText)
        #return Vdiff
        #self.statusBarMessage('Performing SVD of the J matrix')
        self.ORMResultDump += resultText
        
    
#    def matrGrad(self, data, latInfo):
    

    def runORM(self):
        def sendDisplayInfoToMainWindow():
            """ emit the signal to show the dump in the main window """
            self.updatedCalcSignal.emit(self.ORMResultDump)
            
        #TODO: In the ORM cycle for i == 1: calc.matrRdiffnoargs. In the second part: calcMatrRdiff...
        
        def changeQuadStrengths(deltaV):
            """ Takes the file Hesr_CR_injection_StSx.txt to read the quad 
            strengths and creates Hesr_CR_injection_StSx_new.txt to write 
            the new values of the quad strengths (old plus the self.Vdiff)
            """
            readFileEnding = ''
            fileEndingNew = '_new'
            quadStrenNew = self.latParse.errorLattice(self.quadStrengthsNames, deltaV.astype(str), 'absolute', readFileEnding, fileEndingNew)
            return quadStrenNew
            
        def MinSumOfSquares(matrRdiff):
            """ Something like a chi-squared to check how well the algorithm converges """
            sumOfSquaresNew = np.sum(np.square(matrRdiff))
            self.sumOfSquares.append(sumOfSquaresNew)
            resultText = ''
            for i, val in enumerate(self.sumOfSquares):
                if i == 0:
                    resultText = "Iteration: " + str(i) + ". Sum of squares " + " = " + str(round(self.sumOfSquares[i], 5)) + ". It corresponds to 100 %\n"
                    print(resultText)
                else:
                    resultText = "Iteration: " + str(i) + ". Sum of squares " + " = " + str(round(self.sumOfSquares[i], 5)) + ". It corresponds to " +\
                           str(round(self.sumOfSquares[i] / self.sumOfSquares[0] * 100, 2)) + " %\n"
                    print(resultText)
            resultText += '\n*****\n\n'
            self.ORMResultDump += resultText
            
            #print("Sum of squares = ", self.sumOfSquares)
            #print("Epsilon in the next iteration will be ", self.epsilon)
        #ORMResultDump = 'The following quadrupoles'
        sendDisplayInfoToMainWindow()
        for i in range(self.iter):
            if i == 0:
                print('\nIteration number: 1')
                self.madxExec.deleteNewFiles()
                self.madxExec.launchMadx()
                self.twissToData(config.twissFile)
                self.findKickAndMonLengths(self.data['KEYWORD'], self.hkickNames, self.vkickNames)
                self.calcMatrRmodel(config.calcMethod)
                self.measureMatrRexp(config.calcMethod) # this self.matrRexp is used
                self.calcMatrRdiffNoArgs()                  # further in self.calcMatrRdiff
                self.calcJmatrNoArgs()
                self.findQuadErrors()
                changeQuadStrengths(self.Vdiff)
                MinSumOfSquares(self.matrRdiff)
                sendDisplayInfoToMainWindow()
            else:
                print('\nIteration number: ', i+1)
                # new means the model matrix of the next iteration
                matrRnew = self.calcMatrRnew(config.calcMethod, '_new')
                #self.measureMatrRexp()
                #self.matrRdiff = self.calcMatrRdiff(matrRnew, self.matrRexp)
                #self.calcJmatr(matrRnew)
                self.calcMatrRdiff(matrRnew, self.matrRexp)
                self.calcJmatr(matrRnew)
                self.findQuadErrors() # "QuadErrors" is the same as "Vdiff"
                changeQuadStrengths(self.Vdiff)
                MinSumOfSquares(self.matrRdiff)
                sendDisplayInfoToMainWindow()
                
            if config.plotMatrRdiff:
                self.visualORMMatrSignal.emit(self.matrRdiff, i, self.iter)
#                 self.visualORMMatrSignal.emit("asdf")
                #self.visualORMMatr.plotHisto(matr = self.matrRdiff, currentPlot = i, totalPlots = self.iter)
                #self.plotMatrRdiffHisto.plotHisto(self.matrRdiff, i, self.iter)
                
                
        self.ORMResultDump += "\n\nThe calculations are finished!"
        sendDisplayInfoToMainWindow()
        #return self.ORMResultDump
            
            
    

#===============================================================================
# if __name__ == '__main__':
#     twiss = readTwiss(config.twissFile) # Create an instance of a class
#     textRaw = twiss.readFile(config.twissFile)
#     dataRaw, latInfoRaw = twiss.removeJunk(textRaw)
#     data = twiss.textToMatrix(dataRaw)
#     latInfo = twiss.latInfoClean(latInfoRaw) # self.latInfo is a dict
#               
#     calc = calculations()
#     calc.madxExec.deleteNewFiles()
#     calc.runORM()
#     #matrRmodel = calc.matrR(data, latInfo)
#     #matrRexp = calc.matrRexp()
#     #matrRdiff = calc.matrRdiff(matrRmodel, matrRexp)
#     
#===============================================================================
    
if __name__ == '__main__':
    calc = calculations()
    calc.runORM()
    
#     if not os.path.exists(os.path.join(os.getcwd(), config.twissFile)):
#         calc.madxExec.launchMadx()
#     twiss = readTwiss(config.twissFile) # Create an instance of a class
#     
#     textRaw = twiss.readFile(config.twissFile)
#     dataRaw, latInfoRaw = twiss.removeJunk(textRaw)
#     data = twiss.textToMatrix(dataRaw)
#     latInfo = twiss.latInfoClean(latInfoRaw) # self.latInfo is a dict
#                
#     
#     calc.madxExec.deleteNewFiles()
#     calc.madxExec.launchMadx()
#     calc.twissToData(config.twissFile)
#     calc.findKickAndMonLengths(calc.data['KEYWORD'], calc.hkickNames, calc.vkickNames)
#     calc.calcMatrRmodel(config.calcMethod)
#     expmatr = calc.measureMatrRexp(config.calcMethod)

    
#===============================================================================
# if __name__ == '__main__':
#     import matplotlib.pyplot as plt
#     expMatr = np.loadtxt('matrRexpRealSlow.txt')
#     rng = 18
#     plt.plot(range(len(expMatr[0:rng,0])), expMatr[0:rng,29])
#     plt.show()
#     plt.plot(range(len(expMatr[0:rng,0])), expMatr[0:rng,0])
#     plt.show()
#===============================================================================
    

#===============================================================================
# if __name__ == '__main__':
#     twiss = readTwiss(config.twissFile) # Create an instance of a class
#     textRaw = twiss.readFile(config.twissFile)
#     dataRaw, latInfoRaw = twiss.removeJunk(textRaw)
#     data = twiss.textToMatrix(dataRaw)
#     latInfo = twiss.latInfoClean(latInfoRaw) # self.latInfo is a dict
#        
#     calc = calculations()
#     errorVal = 0.001
#     fileEnding = 'xxxxxxx'
#     calc.errorLattice('kkiX1', errorVal, 'absolute', '', fileEnding)   
#     calc.madxExec.launchBat('Launch' + fileEnding + '.bat')
#===============================================================================

