import config
import numpy as np

#TODO: introduce an option to turn off-on the sextupoles
class parseLattice():
    def __init__(self):
        pass
    def readFile(self, fname):
        with open(fname, 'r') as f:
            data = f.readlines()
        return data
        
    def replaceStren(self, data, strenNames, strenErrors, flag):
        def findQuadStren(line):
            idxStart = line.index('=') + 1
            idxEnd = line.rindex(';')
            val = line[idxStart: idxEnd]
            return val
        
        def calcNewValue(strengthValueOld, error, flag):
            if flag == 'relative':
                strengthValueNew = strengthValueOld + ' * (1 + (' + error + '))'
            elif flag == 'absolute':
                precision = 8
                valNewFloat = float(strengthValueOld) + float(error)
                strengthValueNew = str(round(valNewFloat, precision))
            return strengthValueNew   
        
        if isinstance(strenNames, str): # if strenNames contains only one str element: type=string
            # make list with one element, otherwise the 'for' loop works
            # for single letters
            strenNames = [strenNames]
            strenErrors = [strenErrors]
        elif isinstance(strenNames, list): # if strenNames contains multiple elements: type=list
            pass
        else:
            raise Exception("Problem with strenNames!")

        # replace quadrupole strengths
        strenNamesList = strenNames[:] # make a copy of the list
        for i, line in enumerate(data):
            line = line.lower()
            line = line.replace(' ', '')
            if strenNamesList: # if list is not empty
                for strenName, strenError in zip(strenNames, strenErrors):
                    # if strenName in line: # if kq1, it will change also kq10 - incorrect!
                    # if kq1, it will look for kq1:= in the line without spaces
                    
                    if (strenName + ':=') in line or (strenName + '=') in line:
                        strengthValueOld = findQuadStren(line)
                        strengthValueNew = calcNewValue(strengthValueOld, strenError, flag)
                        #lineNew = line.replace(strengthValueOld, strengthValueNew)
                        #TODO: redo the below two lines
                        idx = line.index('=')
                        lineNew = line[0:idx] + '=' + strengthValueNew + ';\n'
                        data[i] = lineNew
                        # We pop the element from the copied list strenNamesList after it is found
                        strenNamesList.remove(strenName)
            elif len(strenNamesList) == 0:
                # If we changed all the quadrupoles and copied list is empty - exit the loop
                break
                
                
        # If some strenErrors with "strengthValueOld" that means that the previous 
        # quadrupole search cycle is erroneous (in replaceQuadStrengths func)
        #FIXME: we return strengthValueOld but it should be strengthValueNew???
        strengthVal = float(strengthValueOld)
                
        return data, strengthVal
    
    def newLatMadx1(self, readFileEnding, writeFileEnding, data):
        """ modify file "Hesr_CR_injection_StSx.txt" to "ending (tmp, new, exp...)"
         and write data to the _ending file """
        #=======================================================================
        # fShortName, ext = readFileName.split('.')
        # fShortNameNew = fShortName + writeFileEnding
        # fullNameNew = fShortNameNew + '.' + ext
        #=======================================================================
        configFirstFileName, configFirstFileExt = config.first.split('.')  # @UndefinedVariable
        readFileName = configFirstFileName + readFileEnding + '.' + configFirstFileExt
        writeFileName = configFirstFileName + writeFileEnding + '.' + configFirstFileExt
        
        configSecondFileName, configSecondFileExt = config.second.split('.')  # @UndefinedVariable
        wordOld = configSecondFileName + readFileEnding + '.' + configSecondFileExt
        wordNew = configSecondFileName + writeFileEnding + '.' + configSecondFileExt
        
        """ Also change the name of the file which is called """
        #print('old', data)
        #DONE: Here if we have at least one blank, than it won't find the string. change it!
        #print('call, file=' + wordOld)
        
        wordCallFound = False
        for i, line in enumerate(data):
            if all(word in line for word in ['call', 'file', wordOld]):
                #print("YEEEES")
                #print('old', data)
                lineNew = line.replace(wordOld, wordNew)
                data[i] = lineNew
                #print('old', data)
                wordCallFound = True
                #print("found it")
        if wordCallFound == False:
            raise Exception("'call, file' string was not found in the first mad-x file")
                
        
        with open(writeFileName, 'w') as fout:
            fout.writelines(data)
        
        #print(wordOld, wordNew)
        #print(writeFileName, data)
            
    def newLatMadx2(self, fullName, ending):
        """ modify file "HesrAperture_CR_core_StSx.txt" so it will output
        twiss_exp.txt instead of twiss.txt"""
        twissName = config.twissFile
        twissMainName, twissExt = twissName.split('.')
        with open(fullName, 'r') as f:
            data = f.readlines()
        for i, line in enumerate(data):
            if twissName in line:
                lineNew = line.replace(twissName, twissMainName + ending + '.' + twissExt)
                data[i] = lineNew
        
        shortName, ext = fullName.split('.')
        shortNameNew = shortName + ending
        fullNameNew = shortNameNew + '.' + ext
        with open(fullNameNew, 'w') as fout:
            fout.writelines(data)
    
    def newLaunchBat(self, fullname, ending):
        """ change file name in Launch.bat to *_exp """
        with open(fullname, 'r') as f:
            data = f.readlines()
        #f.close() # Otherwise we sometimes get an permission denied error in the next 'w' mode
        
        wordOld = config.first
        fileNameMain1, fileExt1 = config.first.split('.')  # @UndefinedVariable
        wordNew = fileNameMain1 + ending + '.' + fileExt1
        for i, line in enumerate(data):
            if wordOld in line:
                data[i] = line.replace(wordOld, wordNew)
        
        shortName, ext = fullname.split('.')
        shortNameNew = shortName + ending
        fullNameNew = shortNameNew + '.' + ext
        
        with open(fullNameNew, 'w') as fout:
            fout.writelines(data)
        fout.close() # Otherwise we sometimes get an permission denied error in the next 'w' mode
        
    def errorLattice(self, strengthNames, errors, errorflag, readFileEnding, writeFileEnding):
        #fileMadx1 = "Hesr_CR_injection_StSx.txt"
        configFirstFileName, configFirstFileExt = config.first.split('.')  # @UndefinedVariable
        #fileMadx1 = 'Hesr_CR_injection_StSx' + readFileEnding + '.txt'
        fileMadx1 = configFirstFileName + readFileEnding + '.' + configFirstFileExt
        dataFileMadx = self.readFile(fileMadx1)
        dataFileMadxNew, strengthValNew = self.replaceStren(dataFileMadx, strengthNames, errors, errorflag)
        self.newLatMadx1(readFileEnding, writeFileEnding, dataFileMadxNew)
        self.newLatMadx2(config.second, writeFileEnding)
        self.newLaunchBat(config.launchBatFile, writeFileEnding)
        
        return strengthValNew
    
    def findKickNamesAndStrengths(self):
        """ Looks for the word 'kicker' in the lattice. When found, extracts 
        the name and the strength of the kicker """
        def stripMultiple(line, endings):
            """ Strips the multiple endings characters from the line"""
            for end in endings:
                if end in line:
                    line = line.replace(end, '')
            return line
    
        def separateKick(data, kickNames, kickStrengths, inputKickType):
            """ looks for either hkickers or vkickers, not the combined ones"""
            for line in data:
                line = line.lower()
                line = line.replace(' ', '')
                if 'kicker' in line:
                    lineValuesDefs = line.split(',')
                    nameDef = lineValuesDefs[0]
                    lineValuesDefs.pop(0) # Remove, we do not need it any longer
                    name, kickType = nameDef.split(':')
                    if kickType == inputKickType:
                        kickNames.append(name)
                        for valDef in lineValuesDefs:
                            if 'kick' in valDef:
                                if ':=' in valDef:
                                    kickType, kickStrengthWithBadEnding = valDef.split(':=')
                                else: # the values are separated by '='
                                    kickType, kickStrengthWithBadEnding = valDef.split('=')
                                kickStrength = stripMultiple(kickStrengthWithBadEnding, [',', '\n', ';'])
                                kickStrengths.append(kickStrength)
            return kickNames, kickStrengths
    
        def combinedKick(data, hkickNames, hkickStrengths, vkickNames, vkickStrengths):
            """ looks for the combined kickers"""
            for line in data:
                    line = line.lower()
                    line = line.replace(' ', '')
                    if 'kicker' in line:
                        lineValuesDefs = line.split(',')
                        nameDef = lineValuesDefs[0]
                        lineValuesDefs.pop(0) # Remove, we do not need it any longer
                        name, type = nameDef.split(':')
                        if type == 'kicker':
                            hkickNames.append(name)
                            vkickNames.append(name)
                            for valDef in lineValuesDefs:
                                if 'hkick' in valDef:
                                    if ':=' in valDef:
                                        kickType, kickStrengthWithBadEnding = valDef.split(':=')
                                    else: # the values are separated by '='
                                        kickType, kickStrengthWithBadEnding = valDef.split('=')
                                    hkickStrength = stripMultiple(kickStrengthWithBadEnding, [',', '\n', ';'])
                                    hkickStrengths.append(hkickStrength)
                                elif 'vkick' in valDef:
                                    if ':=' in valDef:
                                        kickType, kickStrengthWithBadEnding = valDef.split(':=')
                                    else: # the values are separated by '='
                                        kickType, kickStrengthWithBadEnding = valDef.split('=')
                                    vkickStrength = stripMultiple(kickStrengthWithBadEnding, [',', '\n', ';'])
                                    vkickStrengths.append(vkickStrength)
                            
            return hkickNames, hkickStrengths, vkickNames, vkickStrengths
                                    
        hkickNames, hkickStrengths, vkickNames, vkickStrengths = [], [], [], []
        #with open(config.first, 'r') as w:
        #    data = w.readlines()
        data = self.readFile(config.first)
        hkickNames, hkickStrengths = separateKick(data, hkickNames, hkickStrengths, 'hkicker')
        vkickNames, vkickStrengths = separateKick(data, vkickNames, vkickStrengths, 'vkicker')
        hkickNames, hkickStrengths, vkickNames, vkickStrengths = \
            combinedKick(data, hkickNames, hkickStrengths, vkickNames, vkickStrengths)
            
        np.savetxt(config.kickFile, hkickStrengths + vkickStrengths, fmt = '%s')
        return hkickNames, hkickStrengths, vkickNames, vkickStrengths
        
        #===========================================================================
        # print(len(hkickNames), hkickNames)
        # print(len(hkickStrengths), hkickStrengths)
        # print(len(vkickNames), vkickNames)
        # print(len(vkickStrengths), vkickStrengths)
        #===========================================================================
        

if __name__ == '__main__':
    lat = parseLattice()
    
    fname = config.first
    data = lat.readFile(fname)
    for line in data:
        print(line)
    
    # for CR
    quads = ['kq1', 'kq2']
    QuadErrors = ['0.002', '0.002']
    
    #===========================================================================
    # # for HESR
    # quads = ['KQD', 'KQF1']
    # QuadErrors = ['0.001', '-0.002']
    #===========================================================================
    dataNew, quadStrengthVal = lat.replaceStren(data, quads, QuadErrors, flag = 'absolute')
    lat.newLatMadx1(fname, '_exp', dataNew)
    lat.newLatMadx2(fullName = config.second, ending = '_exp')
    lat.newLaunchBat(config.launchBatFile, '_exp')
    
    for line in dataNew:
        print(line)
    
    