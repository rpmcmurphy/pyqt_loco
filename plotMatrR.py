import matplotlib.pyplot as plt
import numpy as np
from mpl_toolkits.mplot3d import Axes3D  # @UnresolvedImport @UnusedImport
import matplotlib.colors as colors

import matplotlib



class histo3d:
    
    def __init__(self):
        pass

    def setupHisto3d(self, dz):
        nx = dz.shape[1]
        ny = dz.shape[0]
        dim = (nx, ny)
        nx, ny = np.arange(nx), np.arange(ny)
        xpos, ypos = np.meshgrid(nx, ny)
        zpos = np.zeros(dim)
        xpos, ypos, zpos = xpos.flatten(), ypos.flatten(), zpos.flatten()
        
        dx, dy = np.ones(dim)/2, np.ones(dim)/2
        # self.dz is a parameter to the function
        # self.dz = np.random.randint(low = 1, high = 10, size = dim)
        dx, dy, dz = dx.flatten(), dy.flatten(), dz.flatten()
        return xpos, ypos, zpos, dx, dy, dz
    
    def setupColormap(self, dz, currentPlot):
        if currentPlot == 0:
            self.firstMax = np.abs(dz.max())
        offset = dz + np.abs(dz.min())
        #print('offset max', offset.max())
        fracs = offset/offset.max()
        #fracs = offset/7
        norm = colors.Normalize(fracs.min(), fracs.max())
        
        """ so far fracs lie in [0; 1]
         we normalize it to the first plot maximum values, so the colormap is the same """
        coef = np.abs(dz.max())/self.firstMax # coef < 1
        #print('coef = ', coef)
        fracs = 0.5 + ((fracs-0.5)*coef)
        #print('fracs min, max', fracs.min(), fracs.max())
        
        cmap = plt.get_cmap('jet')
        #return cmap(norm(fracs))
        return cmap(fracs)

#===============================================================================
#     def plotHisto(self, dz, currentPlot, totalPlots):
#         xpos, ypos, zpos, dx, dy, dz = self.setupHisto3d(dz)
#         cmap = self.setupColormap(dz)
#         
#         #fig = plt.figure(figsize = (12, 9))
#         ax = fig.add_subplot(totalPlots, 1, currentPlot + 1, projection='3d')
#         ax.bar3d(xpos, ypos, zpos, dx, dy, dz, color = cmap, edgecolor = 'none')
#         ax.set_zlim(1.5*dz.min(), 1.5*dz.max())
#         ax.set_xlabel('Monitor number')
#         ax.set_ylabel('Corrector number')
#         ax.set_zlabel('Elements of matrix Rdiff')
#         ax.set_title('Iteration ' + str(currentPlot + 1))
#         
#         #=======================================================================
#         # if currentPlot == totalPlots - 1:
#         #     plt.show()   
#         #=======================================================================
# 
#         #plt.show(block = False)
#===============================================================================
        
class histo3dWindow(object):
    
    def __init__(self, canvas, figure):
        self.orbitCanvas = canvas
        self.orbitFigure = figure
        
        self.histo3dPlot = histo3d()
         
        #self.histo3dPlot = histo3d()
        
        #=======================================================================
        # # for testing
        # nx, ny = 10, 10
        # self.matr = np.random.randint(low = 1, high = 10, size = (nx, ny))
        # 
        # self.xpos, self.ypos, self.zpos, self.dx, self.dy, self.dz = \
        #     self.histo3dPlot.setupHisto3d(self.matr)
        #=======================================================================
        
    def fontSize(self, fontsize = 20):
        font = {'size'   : fontsize}
        matplotlib.rc('font', **font)
        matplotlib.rcParams['axes.labelsize'] = fontsize
        
    def plotHisto(self, matr, currentPlot, totalPlots):
        self.fontSize()
        #histo3dPlot = histo3d()
        
        xpos, ypos, zpos, dx, dy, dz = \
            self.histo3dPlot.setupHisto3d(matr)
            
        #if currentPlot == 0:
        #    self.cmap = histo3dPlot.setupColormap(dz)
        cmap = self.histo3dPlot.setupColormap(dz, currentPlot)
        
        if totalPlots == 1:
            gridCols = 1
        else:
            gridCols = 2
        gridRows = int((totalPlots + totalPlots % 2)/2)
#         print('grid', gridCols, gridRows)
        plotNum = currentPlot + 1
        #print(gridCols, gridRows, plotNum)
        ax = self.orbitFigure.add_subplot(gridCols, gridRows, plotNum, projection='3d')
        #=======================================================================
        # plotRow = int(np.ceil((currentPlot + 1)/2))
        # if currentPlot % 2 == 0:
        #     plotCol = 2
        # elif currentPlot % 2 == 1:
        #     plotCol = 1
        # print(totalPlots, plotCol, plotRow)
        # ax = self.orbitFigure.add_subplot(totalPlots, plotCol, plotRow, projection='3d')
        #=======================================================================
            
            
        #ax = self.orbitFigure.add_subplot(totalPlots, 1, currentPlot + 1, projection='3d')
        #print("here", xpos, ypos, zpos, dx, dy, dz, cmap)
        ax.bar3d(xpos, ypos, zpos, dx, dy, dz, color = cmap, edgecolor = 'none')
        #ax.set_zlim(1.5 * dz.min(), 1.5 * dz.max())
        if currentPlot == 0:
            self.zmin, self.zmax = 1.5 * dz.min(), 1.5 * dz.max()
        ax.set_zlim(self.zmin, self.zmax)
        #labelPad = 10
#         ax.set_xlabel('Monitor number', labelpad = labelPad)
#         ax.set_ylabel('Corrector number', labelpad = labelPad)
        ax.set_xlabel('Corrector, Hor & Ver')
        ax.set_ylabel('Monitor, Hor & Ver')
#         for val in matr:
#             print('xxxx', val)
#         print('****', [val for line in matr for val in line])
        chiSq = np.sum([val**2 for line in matr for val in line])
        ax.set_zlabel('Difference matrix elements')
#         ax.set_title('Iteration ' + str(currentPlot + 1) + '\nchi-squared = ' + str(round(chiSq, 1)))
        if currentPlot == 0:
            pass
        else:
            ax.set_title('After iteration ' + str(currentPlot))
        
        if currentPlot == totalPlots - 1:
            self.orbitCanvas.draw()
            #===================================================================
            # print('I am here')
            # print('xpos', xpos)
            # print('ypos', ypos)
            # print('zpos', zpos)
            # print('dx', dx)
            # print('dy', dy)
            # print('dz', dz)
            # print('cmap', cmap)
            # fig = plt.figure()
            # ax1 = fig.add_subplot(111, projection='3d')
            # ax1.bar3d(xpos, ypos, zpos, dx, dy, dz, color = cmap, edgecolor = 'none')
            # plt.show()
            # #ax.show()
            #===================================================================
    
        
        
    

if __name__ == '__main__':
    nx, ny = 10, 10
    matr = np.random.randint(low = 1, high = 10, size = (nx, ny))
    
    hist = histo3d(matr)
    hist.plotHisto()